$(document).ready(function() {
  // jquery slide

  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:true,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 4,
    pagination: false,
    itemsDesktop      : [1199,4],
    itemsDesktopSmall     : [979,3],
    itemsTablet       : [768,2],
    itemsMobile       : [479,2],
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
	
  $(".slide_img").find(".owl-page").each(function( index, element ) {
    $(this).children('span').text(index+1);
  })

  sync1.trigger('owl.play',2000);

  //Jqurey collapse and see_more

  $(".see_more").hide();

  $(".collapse").click(function(event) {
    $(this).parent().children('li:nth-child(n+5)').hide();
    $(this).hide();
    $(this).parent().children('.see_more').show();
    // $(".see_more").show();
  });

  $(".see_more").click(function(event) {
    $(this).parent().children('li:nth-child(n+5)').show();
    $(this).hide();
    $(this).parent().children('.collapse').show();
    // $(".collapse").show();
  });

  //tab and click see-more product
  var count = $(".product_item:nth-child(1)").find(".product").length ;
  var dem = count-10;
  $(".product_item:nth-child(1)").find(".pro_see_more button").text("Xem thêm "+(count-10)+" sản phẩm");

  $(".product_item:nth-child(1) .pro_see_more button").click(function(event) {
      $(this).parent().parent().children().children('.product:hidden').slice(0, 5).slideDown();
        $(this).text("Xem thêm "+(dem-5)+" sản phẩm");
        dem = dem -5;
        if (($(this).parent().parent().children().children('.product:hidden').length)==0) {
            $(this).fadeOut();
        }
  });
 

  $(".product_item").hide();
  $(".product_item:first-child").show();
  $(".tab_menu nav ul li").click(function(event) {
    $(".tab_menu nav ul li").removeClass('active');
    $(this).addClass('active');
    $(".product_item").hide();
    $(".product_item:nth-child("+($(this).index()+1)+")").show();

    var count = $(".product_item:nth-child("+($(this).index()+1)+")").find(".product").length;
    var dem = count-10;
    if (count <11) {
        $(".product_item:nth-child("+($(this).index()+1)+")").find(".pro_see_more").hide();
    }
    else{
        $(".product_item:nth-child("+($(this).index()+1)+")").find(".pro_see_more button").text("Xem thêm "+(count-10)+" sản phẩm");

        $(".product_item:nth-child("+($(this).index()+1)+") .pro_see_more button").click(function(event) {
              $(this).parent().parent().children().children('.product:hidden').slice(0, 5).slideDown();
               $(this).text("Xem thêm "+(dem-5)+" sản phẩm");
               dem = dem -5;
              if (($(this).parent().parent().children().children('.product:hidden').length)==0) {
               $(this).fadeOut();
          }
        });

      }
  });
  
  //slide product saw
  setTimeout(function(){ 
    $('#product_saw').owlCarousel({
        pagination: false,
        navigation: true,
        items : 5, //10 items above 1000px browser width
        itemsDesktop : [1000,5], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,3], // betweem 900px and 601px
        itemsTablet: [768,2], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    })

    $(".slide_product").find(".owl-prev").text("");
    $(".slide_product").find(".owl-next").text("");

    $('#slide_mb, #slide_mb_2').owlCarousel({
        pagination: false,
        navigation: false,
        items : 1, //10 items above 1000px browser width
        itemsDesktop : [1000,1], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,1], // betweem 900px and 601px
        itemsTablet: [768,1], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    })
  }, 1000);


  //jquery mobile
  $(".remove_banner_mb").click(function(event) {
      $(this).parent().remove();
  });

  // popup stop scroll window
  var $window = $(window), previousScrollTop = 0, scrollLock = false;

  $window.scroll(function(event) {     
    if(scrollLock) {
      $window.scrollTop(previousScrollTop); 
    }

    previousScrollTop = $window.scrollTop();
  });

  //click show popup video
  $(".video_big, .video_small_item").click(function(event) {

    $(".fuzzy").show();
      $("#popup_video").fadeIn(500);
      scrollLock = true;

      $(".left_box_video iframe").attr('src', $(this).attr('href'));
      $(".box_video--title").text($(this).children().children().children(".title_video p span").text());
      return false;

  });

  //click X hide popup video
   jQuery('iframe[src*="https://www.youtube.com/embed/"]').addClass("youtube-iframe");

  $(".close_popup").click(function(event) {
      $(this).parent().hide(500);
      $(".fuzzy").hide();
      scrollLock = false;
      $('.youtube-iframe').each(function(index) {
        $(this).attr('src', $(this).attr('src'));
        return false;
      });

      $(".left_box_video iframe").attr('src', '');
  });

  //click ".fuzzy" hide popup video
  $(".fuzzy").click(function(event) {
      $(this).hide();
      $("#popup_video").fadeOut(500);
       scrollLock = false;
      $('.youtube-iframe').each(function(index) {
        $(this).attr('src', $(this).attr('src'));
        return false;
      });
      $(".left_box_video iframe").attr('src', '');
  });

  // count-down
 
  $('[data-countdown]').each(function() {
    var $this = $(this), finalDate = $(this).data('countdown');
    $this.countdown(finalDate, function(event) {
      $this.html(event.strftime("Còn lại: %H:%M:%S'"));
      // $this.html(event.strftime("Còn lại: %D days %H:%M:%S'"));
    });
  });
  

  

});