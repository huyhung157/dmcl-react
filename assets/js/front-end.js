


window.$ = window.jQuery= require("jquery");

require("./homepage.js");
require("./cat-con.js");
require("./detail-product.js");

require("owlcarousel/owl-carousel/owl.carousel.min.js");
require("jquery-countdown/dist/jquery.countdown.min.js");
require("jquery-mask-plugin/dist/jquery.mask.js");

require("star-rating-svg/dist/jquery.star-rating-svg.js");

