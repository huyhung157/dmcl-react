jQuery(document).ready(function($) {

  setTimeout(function(){ 

  	var sync1 = $("#sync3");
    var sync2 = $("#sync4");
   
    sync1.owlCarousel({
      singleItem : true,
      slideSpeed : 1000,
      navigation: false,
      pagination:false,
      afterAction : syncPosition,
      responsiveRefreshRate : 200,
    });
   
    sync2.owlCarousel({
      items : 5,
      pagination: false,
      navigation: true,
      itemsDesktop      : [1199,5],
      itemsDesktopSmall     : [979,4],
      itemsTablet       : [768,4],
      itemsMobile       : [479,4],
      responsiveRefreshRate : 100,
      afterInit : function(el){
        el.find(".owl-item").eq(0).addClass("synced");
      }
    });
   
    function syncPosition(el){
      var current = this.currentItem;
      $("#sync4")
        .find(".owl-item")
        .removeClass("synced")
        .eq(current)
        .addClass("synced")
      if($("#sync4").data("owlCarousel") !== undefined){
        center(current)
      }
    }
   
    $("#sync4").on("click", ".owl-item", function(e){
      e.preventDefault();
      var number = $(this).data("owlItem");
      sync1.trigger("owl.goTo",number);
    });
   
    function center(number){
      var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
      var num = number;
      var found = false;
      for(var i in sync2visible){
        if(num === sync2visible[i]){
          var found = true;
        }
      }
   
      if(found===false){
        if(num>sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", num - sync2visible.length+2)
        }else{
          if(num - 1 === -1){
            num = 0;
          }
          sync2.trigger("owl.goTo", num);
        }
      } else if(num === sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", sync2visible[1])
      } else if(num === sync2visible[0]){
        sync2.trigger("owl.goTo", num-1)
      }
      
    }
    $(".box_pro-images-small").find(".owl-prev").text("");
    $(".box_pro-images-small").find(".owl-next").text("");
}, 1000);
  //Show them tính năng
  $(".feature_see_more").click(function(event) {
    $(".feature_item p:nth-child(n+5)").slideDown();
    $(this).fadeOut();
  });

  //rating-star
  $(".my-rating").starRating({
    starSize: 15,
    totalStars: 5,
    starShape: 'rounded',
    activeColor: '#efce4a',
    useGradient: false
  });

  // select-price
  $(".size_pro").find("p strong:nth-child(2)").text($(".one_size.active").find(".size-pro").text());

  $(".one_size").change(function(event) {
    $(".one_size").removeClass('active');
    $(this).addClass('active');
    var number_inch = $(this).find(".size-pro").text();
    $(".size_pro").find("p strong:nth-child(2)").text(number_inch);
  });

  //slide-product
  setTimeout(function(){ 
    $('#slide_pro_price, #slide_pro').owlCarousel({
        pagination: false,
        navigation: true,
        items : 5, //5 items above 1000px browser width
        itemsDesktop : [1000,5], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,5], // betweem 900px and 601px
        itemsTablet: [768,2], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    })

    $(".slide_product").find(".owl-prev").text("");
    $(".slide_product").find(".owl-next").text("");
   }, 1000);

  //tab thông tin sản phẩm
  $(".info_pro-title ul li").click(function(event) {
      $(".info_pro-title ul li").removeClass('active');
      $(this).addClass('active');

      $(".info_pro-item").removeClass('current');
      $(".info_pro-item:nth-child("+($(this).index()+1)+")").addClass('current');
  });

  //Xem thêm thông tn sản phẩm
  $(".see_specifications").click(function(event) {
      $(".list_specifications").addClass('show_detail_specifications');
      $(this).fadeOut();
  })

  $(".see_feature").click(function(event) {
      $(".des_pro").addClass('show_des_pro');
      $(this).fadeOut();
  });

  $(".see_videos").click(function(event) {
      $(this).parent().parent().children(".v_small:hidden").slice(0, 3).slideDown();
      if (($(this).parent().parent().children(".v_small:hidden")).length ==0) {
        $(this).fadeOut();
      }
      
  });
  //jquery video
 
  $(".v_img_video a").click(function(event) {
    $(".video_banner iframe").attr('src',''+ $(this).attr("href")+'?autoplay=1&enablejsapi=1&rel=0');
    $(".video_banner h3").text($(this).parent().parent().children().children('.info_video a').text());
    $(".video_banner p").text($(this).parent().parent().children().children('.info_video span').text())

    return false;
  });

  $(".info_video a").click(function(event) {
    $(".video_banner iframe").attr('src', ''+ $(this).attr("href")+'?autoplay=1&enablejsapi=1&rel=0');
    $(".video_banner h3").text($(this).text());
    $(".video_banner p").text($(this).parent().children(".info_video span").text());
    return false;
  });

  $(".star_bar").each(function(){
      $(this).find('.star_barsub').animate({
        width: $(this).attr('data-percent')
      }, 3000);
  });

  //slide img_customer
  setTimeout(function(){ 
    $('#img_cus').owlCarousel({
        pagination: false,
        navigation: true,
        items : 7, //5 items above 1000px browser width
        itemsDesktop : [1000,6], //5 items between 1000px and 901px
        itemsDesktopSmall : [900,5], // betweem 900px and 601px
        itemsTablet: [768,3], //2 items between 600 and 0
        itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    })
    $(".img_cus").find(".owl-prev").text("");
    $(".img_cus").find(".owl-next").text("");
  }, 1000);

  //jquery comment
  $(".comment_active").hide();
    $(".comment").click(function(event) {
      $(this).parent().parent().children('.comment_active').slideDown();
    });
    $(".cancel").click(function(event) {
      $(this).parent().parent(".comment_active").slideUp();
    });

    $(".like ").click(function(event) {
      $(this).children('.icon_like').toggleClass('active');
    });

    $(".dislike ").click(function(event) {
      $(this).children('.icon_dislike').toggleClass('active');
    });


    $(".see_more_comment").click(function(event) {
      $(this).parent().children('.comment_ask:hidden').slice(0, 5).slideDown();
      if (($(this).parent().children('.comment_ask:hidden')).length==0) {
        $(this).fadeOut();
      }
    });

    $(".see_more_write p").click(function(event) {
      $(this).parent().parent().children().children('.one_exp:hidden').slice(0, 5).slideDown();
      if (($(this).parent().parent().children().children('.one_exp:hidden')).length==0) {
        $(this).fadeOut();
      }
    });


    //jquery scroll
    if (screen.width > 768) {
      if ($("div").hasClass('experience')) {
        $(window).scroll(function(event) {
          var position_body = $("html,body").scrollTop();
          var position_start = $(".content_eva_exp").offset().top;
          var position_end = $(".info_pro_child").offset().top-500;
          if ((position_body >= position_start) && (position_body < position_end) ) {
            $(".experience").addClass('experience_fixed');
          } else{
            $(".experience").removeClass('experience_fixed');
          }
        });
      }
    }

    //slide popup-images
    setTimeout(function(){ 

      var sync1 = $("#slide_popup-img-big");
      var sync2 = $("#slide_popup-img-small");

      sync1.owlCarousel({
        singleItem : true,
        slideSpeed : 1000,
        navigation: true,
        pagination:false,
        afterAction : syncPosition,
        responsiveRefreshRate : 200,
      });

      sync2.owlCarousel({
        items : 12,
        pagination: false,
        navigation: false,
        itemsDesktop      : [1199,12],
        itemsDesktopSmall     : [979,10],
        itemsTablet       : [768,5],
        itemsMobile       : [479,5],
        responsiveRefreshRate : 100,
        afterInit : function(el){
          el.find(".owl-item").eq(0).addClass("synced");
        }
      });

      function syncPosition(el){
        var current = this.currentItem;
        $("#slide_popup-img-small")
        .find(".owl-item")
        .removeClass("synced")
        .eq(current)
        .addClass("synced")
        if($("#slide_popup-img-small").data("owlCarousel") !== undefined){
          center(current)
        }
      }

      $("#slide_popup-img-small").on("click", ".owl-item", function(e){
        e.preventDefault();
        var number = $(this).data("owlItem");
        sync1.trigger("owl.goTo",number);
      });

      function center(number){
        var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
        var num = number;
        var found = false;
        for(var i in sync2visible){
          if(num === sync2visible[i]){
            var found = true;
          }
        }

        if(found===false){
          if(num>sync2visible[sync2visible.length-1]){
            sync2.trigger("owl.goTo", num - sync2visible.length+2)
          }else{
            if(num - 1 === -1){
              num = 0;
            }
            sync2.trigger("owl.goTo", num);
          }
        } else if(num === sync2visible[sync2visible.length-1]){
          sync2.trigger("owl.goTo", sync2visible[1])
        } else if(num === sync2visible[0]){
          sync2.trigger("owl.goTo", num-1)
        }

      }
      $(".slide_popup-img-big").find(".owl-prev").text("");
      $(".slide_popup-img-big").find(".owl-next").text("");
    }, 1000);


    $(".see_zoom-img").click(function(event) {
      $("#popup_images").addClass('show');
    });
    $(".button-close-popup").click(function(event) {
      $(this).parent().parent("#popup_images").removeClass('show');
      return false;
    });

    $(".see_zoom-video").click(function(event) {
      $("#popup_videos").addClass('show');

      $("#youtubeVideo").attr('src', ''+($(".slide_popup-video-small .item.active").attr("href"))+'?autoplay=1&enablejsapi=1&rel=0');
    });


    $(".button-close-popup").click(function(event) {
      $(this).parent().parent("#popup_videos").removeClass('show');
      $("#youtubeVideo").attr('src','');
      return false;
      
    });

    $(".slide_popup-video-item .item").click(function(event) {
      $(".slide_popup-video-item .item").removeClass('active');
      $(this).addClass('active');
      $("#youtubeVideo").attr('src', ''+ $(this).attr("href")+'?autoplay=1&enablejsapi=1&rel=0');
      return false;
    });

    //popup number_market
    $(".adress_stock").click(function(event) {
      $(".popup_st_parent").addClass('show_popup_st');
      return false;
    });

    $(".button-close-popup").click(function(event) {
      $(this).parent().parent().parent(".popup_st_parent").removeClass('show_popup_st');
    });

    $(".fuzzy_st").click(function(event) {
     $(this).parent(".popup_st_parent").removeClass('show_popup_st');
    });

    //js number_market
    $("#area").change(function(event) {
      $(".view_brand").hide();
      $(".view_brand input").prop("checked",false);

      $(".view_brand.area_"+($(this).val())+"").show();

      $(".view_brand.area_"+($(this).val())+" .checked").prop("checked",true);
      if ($(this).val()==0) {
        $(".view_brand").show();
        $(".view_brand:first-child .checked").prop("checked",true);
        changeMarkerPos( $(".view_brand:first-child .checked").data('longitude'),  $(".view_brand:first-child .checked").data('latitude'));
      }
      changeMarkerPos( $(".view_brand.area_"+($(this).val())+" .checked").data('longitude'),  $(".view_brand.area_"+($(this).val())+" .checked").data('latitude'));

      $(".info_area").animate({
          scrollTop: 0
      }, 800)
    });

    $("#county").change(function(event) {
      $(".view_brand").hide();
      $(".view_brand.view_brand_t_"+($(this).val())+"").show();
      $(".view_brand.view_brand_t_"+($(this).val())+" input").prop("checked",true);

      changeMarkerPos( $(".view_brand.view_brand_t_"+($(this).val())+" input").data('longitude'),  $(".view_brand.view_brand_t_"+($(this).val())+" input").data('latitude'));
    });





  var marker;
  var map;
  $(".view_brand input").change(function() {
     changeMarkerPos($(this).data('longitude'), $(this).data('latitude'));
  });




  function initialize() {
    var styles = [{
      stylers: [{
        saturation: -100
      }]
    }];
    var styledMap = new google.maps.StyledMapType(styles, {
      name: "Styled Map"
    });
    var mapProp = {
      center: new google.maps.LatLng(10.75419,106.664557),
      zoom: 18,
      panControl: true,
      zoomControl: true,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      overviewMapControl: false,
      rotateControl: true,
      scrollwheel: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), mapProp);
    
    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style')

    marker = new google.maps.Marker({
      position: new google.maps.LatLng(10.75419,106.664557),
      animation: google.maps.Animation.DROP,
    });
    
    marker.setMap(map);
    map.panTo(marker.position);
  }

  function changeMarkerPos(lat, lon){
    myLatLng = new google.maps.LatLng(lat, lon)
    marker.setPosition(myLatLng);
    map.panTo(myLatLng);
  }

  google.maps.event.addDomListener(window, 'load', initialize);


  
});
