$(document).ready(function() {
  $('.cus_money').mask('000.000.000.000.000', {reverse: true});

  setTimeout(function(){ 
  	$('#promo_pro_sale, #promo_pro_monopoly, #promo_pro_selling').owlCarousel({
  	      pagination: false,
  	      navigation: true,
  	      items : 5, //5 items above 1000px browser width
  	      itemsDesktop : [1000,5], //5 items between 1000px and 901px
  	      itemsDesktopSmall : [900,5], // betweem 900px and 601px
  	      itemsTablet: [768,2], //2 items between 600 and 0
  	      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  	})
  }, 1000);
  setTimeout(function(){ 
    $('#slide_banner').owlCarousel({
          pagination: false,
          navigation: true,
          items : 3, //5 items above 1000px browser width
          itemsDesktop : [1000,3], //5 items between 1000px and 901px
          itemsDesktopSmall : [900,3], // betweem 900px and 601px
          itemsTablet: [768,2], //2 items between 600 and 0
          itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
    })
    $(".slide_product_cat, .slide_banner ").find(".owl-prev").text("");
    $(".slide_product_cat, .slide_banner").find(".owl-next").text("");
  }, 1000);
	  
  
  	$(".tab_promo_cat nav ul li").click(function(event) {
  		$(".tab_promo_cat nav ul li").removeClass('current')
  		$(this).addClass('current');
  		$(".list_pro_cat-item").removeClass('active');
  		$(this).parent().parent().parent().parent().children().children(".list_pro_cat-item:nth-child("+($(this).index()+1)+")").addClass('active');
  	});

  	//đếm số sản phẩm hiển thị
	  var count = $(".list_product_cat").find(".product").length ;
  	var dem = count-16;
  	$(".see_more_cat button").text("Xem thêm "+(count-16)+" sản phẩm");

  	$(".see_more_cat button").click(function(event) {
      	$(this).parent().parent().children().children('.product:hidden').slice(0, 4).slideDown();
        	$(this).text("Xem thêm "+(dem-4)+" sản phẩm");
        	dem = dem -4;
        	if (($(this).parent().parent().children().children('.product:hidden').length)==0) {
            	$(this).fadeOut();
        	}
  	});

  	//Bô lọc sản phẩm
    $(".delete_all").hide();
    if(Number( $(".cat_search-item").length)>0){
      $(".delete_all").fadeIn();
    }
    
    //Xóa tất cả bộ lọc
  	$(".delete_all").click(function(event) {
  		$(this).parent().children().children('.cat_search-item').remove();
      $(this).fadeOut();

      $(".cat_brand-item input").prop("disabled",false);
      $(".cat_brand-item input").prop("checked",false);

      $(".cat_price-item input").prop("disabled",false);
      $(".cat_price-item input").prop("checked",false);

      $(".cat_range-item input").prop("disabled",false);
      $(".cat_range-item input").prop("checked",false);

      $(".cat_species-item input").prop("disabled",false);
      $(".cat_species-item input").prop("checked",false);

      $(".cat_resolution-item input").prop("disabled",false);
      $(".cat_resolution-item input").prop("checked",false);
      
      $(".cat_utilities-item input").prop("disabled",false);
      $(".cat_utilities-item input").prop("checked",false);

     
      
  	});


    //Bộ lọc brand
  	$(".cat_brand-item").change(function(event) {
      $(".delete_all").fadeIn();

      if ($(this).children().children('input').is(':checked')) {
         $(".list_cat_search").append('<span class="cat_search-item"><span>'+ ($(this).data('name')) +'</span><i>x</i></span>');
      }
      else{
        var check_brand = $(this).data('name');
        $(".cat_search-item span").each(function( index, element ) {
          var check_cat_search = $(this).text();
          if (check_brand == check_cat_search) {
            $(".cat_search-item:nth-child("+(index+1)+") span").parent().remove();
          }
        })
      }

      if(($(".cat_search-item").length) == 0){
          $(".delete_all").fadeOut();
      }

  	});

     //Bộ lọc price
    $(".cat_price-item").change(function(event) {
      $(".delete_all").fadeIn();

      if ($(this).children().children('input').is(':checked')) {
         $(".list_cat_search").append('<span class="cat_search-item"><span>'+ ($(this).find("span:first-child").text()) +'</span><i>x</i></span>');
      }
      else{
        var check_price = $(this).find("span:first-child").text();
        $(".cat_search-item span").each(function( index, element ) {
          var check_cat_search = $(this).text();
          if (check_price == check_cat_search) {
            $(".cat_search-item:nth-child("+(index+1)+") span").parent().remove();
          }
        })
      }
      if(($(".cat_search-item").length) == 0){
          $(".delete_all").fadeOut();
      }
    });

    
    $(".price_desire-button").click(function(event) {
      setTimeout(function(){ 
         $('.cus_money').mask('000.000.000.000.000', {reverse: true});
      }, 500);
     
      $(".delete_all").fadeIn();
      if ((( $("#price_start").val()) != "") && (( $("#price_end").val()) != "")) {
        var price_start = parseInt($("#price_start").val().replace(".", "").replace(".", ""));
        var price_end = parseInt($("#price_end").val().replace(".", "").replace(".", ""));
        if (Number(price_start) < Number(price_end)) {
            $(".list_cat_search").append('<span class="cat_search-item"><span class="cus_money">'+ price_start +'</span>Đ - <span class="cus_money">'+ price_end +'</span>Đ<i>x</i></span>');
        }
        else{
            alert("Giá tối thiểu phải nhỏ hơn giá đa");
        }
          
      }
      else{
        alert("Vui lòng nhập đầy đủ giá cần tìm.");
      }

      $("#price_start").val('');
      $("#price_end").val('');
        
    });

    //Bộ lọc size 
    $(".cat_range-item").change(function(event) {
      $(".delete_all").fadeIn();

      if ($(this).children().children('input').is(':checked')) {
         $(".list_cat_search").append('<span class="cat_search-item"><span>'+ ($(this).data('name')) +'</span><i>x</i></span>');
      }
      else{
        var check_range = $(this).data('name');
        $(".cat_search-item span").each(function( index, element ) {
          var check_cat_search = $(this).text();
          if (check_range == check_cat_search) {
            $(".cat_search-item:nth-child("+(index+1)+") span").parent().remove();
          }
        })
      }
      if(($(".cat_search-item").length) == 0){
          $(".delete_all").fadeOut();
      }


    });

    $(".search_inch-button").click(function(event) {
      $(".delete_all").fadeIn();
      var number_inch = $("#number_inch").val();
      if (number_inch != "") {
          $(".list_cat_search").append('<span class="cat_search-item"><span>'+ number_inch +' inch</span><i>x</i></span>');
      }
      else{
        alert("Vui lòng nhập số inch cần tìm");
      }
       $("#number_inch").val('');
    });

    //Bộ lọc loại 
    $(".cat_species-item").change(function(event) {
      $(".delete_all").fadeIn();
      if ($(this).children().children('input').is(':checked')) {
         $(".list_cat_search").append('<span class="cat_search-item"><span>'+ ($(this).find("span:first-child").text()) +'</span><i>x</i></span>');
      }
      else{
        var check_species = $(this).find("span:first-child").text();
        $(".cat_search-item span").each(function( index, element ) {
          var check_cat_search = $(this).text();
          if (check_species == check_cat_search) {
            $(".cat_search-item:nth-child("+(index+1)+") span").parent().remove();
          }
        })
      }
      if(($(".cat_search-item").length) == 0){
          $(".delete_all").fadeOut();
      }

    });

    //Bộ lọc độ phân giải
    $(".cat_resolution-item").change(function(event) {
      $(".delete_all").fadeIn();
      
      if ($(this).children().children('input').is(':checked')) {
         $(".list_cat_search").append('<span class="cat_search-item"><span>'+ ($(this).find("span:first-child").text()) +'</span><i>x</i></span>');
      }
      else{
        var check_resolution = $(this).find("span:first-child").text();
        $(".cat_search-item span").each(function( index, element ) {
          var check_cat_search = $(this).text();
          if (check_resolution == check_cat_search) {
            $(".cat_search-item:nth-child("+(index+1)+") span").parent().remove();
          }
        })
      }
      if(($(".cat_search-item").length) == 0){
          $(".delete_all").fadeOut();
      }

    });

    //Bộ lộc tiện ích
    $(".cat_utilities-item").change(function(event) {
      $(".delete_all").fadeIn();

      if ($(this).children().children('input').is(':checked')) {
         $(".list_cat_search").append('<span class="cat_search-item"><span>'+ ($(this).find("span:first-child").text()) +'</span><i>x</i></span>');
      }
      else{
        var check_utilities = $(this).find("span:first-child").text();
        $(".cat_search-item span").each(function( index, element ) {
          var check_cat_search = $(this).text();
          if (check_utilities == check_cat_search) {
            $(".cat_search-item:nth-child("+(index+1)+") span").parent().remove();
          }
        })
      }
      if(($(".cat_search-item").length) == 0){
          $(".delete_all").fadeOut();
      }

    });


    //Xóa tab bộ lọc
    $(document).on("click", ".cat_search-item i" , function() {
          $(this).parent().remove();

          var demo = $(this).parent().children(".cat_search-item span").text();

          $(".cat_brand-item").each(function( index, element ) {
              var brand = $(this).data('name');
              if (demo == brand) {
                  $(".cat_brand-item:nth-child("+(index+1)+") input").prop("disabled",false);
                  $(".cat_brand-item:nth-child("+(index+1)+") input").prop("checked",false);
              }
          })

          $(".cat_price-item").each(function( index, element ) {
              var price = $(this).find("span:first-child").text();
              if (demo == price) {
                  $(".cat_price-item:nth-child("+(index+1)+") input").prop("disabled",false);
                  $(".cat_price-item:nth-child("+(index+1)+") input").prop("checked",false);
              }
          })

          $(".cat_range-item").each(function( index, element ) {
              var size = $(this).data('name');
              if (demo == size) {
                  $(".cat_range-item:nth-child("+(index+1)+") input").prop("disabled",false);
                  $(".cat_range-item:nth-child("+(index+1)+") input").prop("checked",false);
              }
          })

          $(".cat_species-item").each(function( index, element ) {
              var price = $(this).find("span:first-child").text();
              if (demo == price) {
                  $(".cat_species-item:nth-child("+(index+1)+") input").prop("disabled",false);
                  $(".cat_species-item:nth-child("+(index+1)+") input").prop("checked",false);
              }
          })

          $(".cat_resolution-item").each(function( index, element ) {
              var resolution = $(this).find("span:first-child").text();
              if (demo == resolution) {
                  $(".cat_resolution-item:nth-child("+(index+1)+") input").prop("disabled",false);
                  $(".cat_resolution-item:nth-child("+(index+1)+") input").prop("checked",false);
              }
          })
          $(".cat_utilities-item").each(function( index, element ) {
              var price = $(this).find("span:first-child").text();
              if (demo == price) {
                  $(".cat_utilities-item:nth-child("+(index+1)+") input").prop("disabled",false);
                  $(".cat_utilities-item:nth-child("+(index+1)+") input").prop("checked",false);
              }
          })

          var number_tab = $(".cat_search-item").length;
          if (Number(number_tab) == 0) {
             $(".delete_all").fadeOut();
          }

    });  

    // sort list
    $(".sort_one").click(function(event) {
      $(this).addClass('active');
      $(".sort_two").removeClass('active');

      $(".list_product_cat").removeClass('list_horizontal');

      var count1 = $(".list_product_cat").find(".product:hidden").length ;
      $(".see_more_cat button").text("Xem thêm "+(count1)+" sản phẩm");

      $(".see_more_cat button").click(function(event) {
        $(this).parent().parent().children().children('.product:hidden').slice(0, 4).slideDown();
        $(this).text("Xem thêm "+(count1-4)+" sản phẩm");
        count1 = count1 -4;
        if (($(this).parent().parent().children().children('.product:hidden').length)==0) {
          $(this).fadeOut();
        }
      });
    });

    $(".sort_two").click(function(event) {
      $(this).addClass('active');
      $(".sort_one").removeClass('active');

      $(".list_product_cat").addClass('list_horizontal');

    });

});