import React, { Component } from 'react';
import ProductItem from '../productItem/productItem.component';
import data from '../../data'
class ProductList extends Component {

    renderList() {
        return data.productList.map((item, index) => {
            return (
                <ProductItem key={index} item={item} />
            )
        })
    }

    render() {
        return (
            <div>
                <div className="max-width purchased-together">
                    <h3 className="title_product">Sản phẩm thường mua cùng</h3>
                    <div className="slide_product owl-carousel owl-theme" id="slide_pro_price">
                        {this.renderList()}
                    </div>
                </div>
            </div>
        );
    }
}

export default ProductList;
