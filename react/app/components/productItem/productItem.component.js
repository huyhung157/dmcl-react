import React, { Component } from 'react';

class ProductItem extends Component {

    renderProduct(item) {

    }

    renderImgProduct(item) {
        if (item) {
            return (
                <div className="img_pro">
                    <a href={item.linkProduct}>
                        <img src={item.imgProductUrl} alt={item.nameProduct.productCode} />
                    </a>
                </div>
            )
        }
    }

    renderLabelBuy(label) {
        if (label) {
            return <p class="icon_buy">Nên mua</p>

        }
    }

    renderLabelSelling(label) {
        if (label) {
            return <p class="icon_selling">Bán chạy <span>#01</span></p>
        }
    }

    renderNameProduct(item) {
        let nameProduct = item.nameProduct[0];
        if (nameProduct) {
            return (
                <div className="name_pro">
                    <a href={item.linkProduct}>
                        <img src={nameProduct.imgBrand} alt={nameProduct.productCode} className="icon_trademark" />
                    </a>
                    <a href={item.linkProduct}>
                        <span>{nameProduct.productCode}</span>
                        <h3>{nameProduct.poductControll}</h3>
                    </a>
                </div>
            )
        }
    }

    renderPrice(price) {
        if (price) {
            return (
                <div className="price_sale">
                    <span>{price}đ</span>
                </div>
            )
        }
    }

    renderDiscount(discount) {
        if (discount) {
            return (
                <>
                    <span class="price_market">{discount[0].promotionPrice}đ</span>
                    <span class="discount_percent">-{discount[0].percentDiscount}%</span>
                </>
            )
        }
    }

    renderGift(gift) {
        if (gift) {
            return <span>{gift}</span>
        }
    }

    renderGiftDetail(gift) {
        if (gift) {
            return <span>{gift}</span>

        }
    }


    render() {
        const { item } = this.props;
        return (
            <div>

                <div className="product">
                    <div className="lable_pro">
                        {this.renderLabelBuy(item.labelBuy)}
                        {this.renderLabelSelling(item.labelSelling)}
                    </div>
                    {this.renderImgProduct(item)}
                    <div className="pro_promo">
                        {/* <div class="icon_flash">
                                  <p>Siêu sale ngày rực</p>
                              </div> */}
                    </div>
                    {this.renderNameProduct(item)}
                    {this.renderPrice(item.price)}
                    <div class="discount">
                        {this.renderDiscount(item.discount)}
                    </div>
                    <div className="gift">
                        {this.renderGift(item.gift)}
                    </div>
                    <div className="gift_detail">
                        {this.renderGiftDetail(item.giftDetail)}
                    </div>
                    {/* <div class="evaluate_compare">
                                  <span class="evaluate"><i></i>Đánh giá 8.7/10</span>
                                  <a href="" class="compare">So sánh</a>
                    </div> */}
                </div>

            </div>
        );
    }
}

export default ProductItem;
