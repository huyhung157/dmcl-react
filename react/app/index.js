import React from 'react';
import ReactDOM from 'react-dom';
import './App.scss';
import App from './App';


import "bootstrap/dist/css/bootstrap.min.css";
import "jquery/dist/jquery.min.js";
import "jquery/dist/jquery.slim.js";
import "popper.js/dist/umd/popper.min.js";
import "bootstrap/dist/js/bootstrap.min.js";

ReactDOM.render(
    <App />,
    document.getElementById("root")
);

