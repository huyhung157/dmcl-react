import React, { Component } from 'react';
import ProductList from './components/productList/productList.component';
class App extends Component {
    render() {
        return (
            <div className="app">
                <ProductList />
            </div>
        );
    }
}

export default App;
