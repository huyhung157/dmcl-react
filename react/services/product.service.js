import Axios from 'axios';
import * as API from './config';

class ProductService {
    fetchListProduct(hash) {
        return Axios({
            method: 'GET',
            url: `${API.urlAPI}api/get-${hash}`,
        })
    }
}
export default ProductService;