<!DOCTYPE html>
<html lang="vi-VN">
<head>
	<meta charset="UTF-8">
	<title>Chi Tiết Sản Phẩm</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content=" Siêu thị điện máy Chợ Lớn ">
	<meta name="Author" content="Siêu thị Điện máy Chợ Lớn">
	<meta name="description" content="Mua đồ điện máy, gia dụng tại Điện Máy Chợ Lớn ✓ Bao giá 35 ngày ✓ Hoàn tiền nếu nơi khác rẻ hơn ✓ Trả góp 0% ✓ Hàng chính hãng từ tập đoàn Samsung, LG, Sony...">

<link rel="stylesheet" href="public/css/front-end.css">
<link rel="stylesheet" href="public/css/slider-product-react.css">


</head> 
<body>
	<!-- Header -->
	<header>
		<div class="img_header">
			<a href="">
				<img src="assets/img/hompage_01.png" alt="ảnh header" >
			</a>
		</div>
		<div class="info_header">
			<div class="max-width">
				<a href="" >
					<span><i class="icon_login"></i>Đăng ký / Đăng nhập</span>
				</a>
				<a href="">
					<span>Góp ý & phản hồi</span>
				</a>
				<a href="">
					<span>Chính sách bảo hành & 35 ngày đổi trả</span>
				</a>
				<a href="">
					<span>Hệ thống 71 chi nhánh</span>
				</a>
			</div>
		</div>
		<div class="max-width">
			<div class="list_utilities">
				<div class="logo">
					<a href=""><img src="assets/img/logo.png" alt="ảnh logo"></a>
				</div>
				<div class="utilities">
					<div class="search_bar">
						<form action="">
							<div class="search_bar_item">
								<input type="text" value="" name="search_bar" placeholder="Tìm kiếm sản phẩm...">
								<button type="submit" class="button_search"></button>
							</div>
						</form>
					</div>
					<a href="" class="follow_order">
						<span><i></i>Theo dõi đơn hàng</span>
					</a>
					<a href="" class="cart">
						<span>
							<i><small>19</small></i>Giỏ hàng
						</span>
					</a>
					<a href="tel:02838563388" class="hotline">
						<span class="hotline_item">
							<i></i>
							<span class="hotline_child">Hotline bán hàng <strong>028.38563388</strong></span>
						</span>
					</a>
				</div>
			</div>
		</div>
	</header>
	<section class="content">
		<!-- catogery -->
		<div class="categories_parent">
			<div class="max-width">
				<div class="categories">
					<div class="list_menu">
						<i></i>
						<span>Danh mục</span>
						<div class="menu_hover">
							<div class="info_menu">
								<div class="menu_parent">
									<nav >
										<ul class="menu">
											<li class="menu_item">
												<a href="" class="menu_item_child">
													<i class="icon_tivi"></i>
													<span>Sản phẩm điện tử</span>
													<i class="arrow"></i>
												</a>
												<div class="dropdown_menu">
													<div class="dropdown_menu_parent">
														<div class="dropdown_menu_item">
															<nav >
																<ul class="dropdown_menu_child">
																	<li>
																		<a href="" class="cat" title="Tivi LED/OLED/QLED"> Tivi LED/OLED/QLED <i></i></a>
																		<nav class="trademark">
																			<ul>
																				<li><strong>THƯƠNG HIỆU</strong></li>
																				<li>
																					<a href="" class="cat_item">Sharp <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">AQUA <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Samsung <i></i></a>
																					<nav class="feature">
																						<ul>
																							<li><strong>TÍNH NĂNG</strong></li>
																							<li><a href="">Side by side</a></li>
																							<li><a href="">Inverter</a></li>
																							<li><a href="">Multi doors</a></li>
																							<li><a href="">Ngăn đá trên</a></li>
																							<li><a href="">Ngăn đá dưới</a></li>
																							<li><a href="">Mini</a></li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																						<ul>
																							<li><strong>DUNG TÍCH</strong></li>
																							<li><a href="">Dưới 150L</a></li>
																							<li><a href="">150 - 300L</a></li>
																							<li><a href="">300 - 450L</a></li>
																							<li><a href="">Trên 450L</a></li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																					</nav>
																				</li>
																				<li>
																					<a href="" class="cat_item">Pannasonic <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sony <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Toshiba <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Electrolux <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">LG <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Hitachi <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Alaska <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sanaky <i></i></a>
																				</li>
																				<div class="collapse">Thu gọn</div>
																				<div class="see_more">Xem thêm</div>
																			</ul>
																		</nav>
																	</li>
																	<li>
																		<a href="" class="cat" title="Dàn máy/loa thanh/ loa cong/ dàn âm thanh "> Dàn máy <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Loa thanh (Soundbar), soundbar, loa thanh giá rẻ, loa khuyen mai"> Loa thanh (Soundbar) <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Loa/loa kéo/loa du lịch/loa ngang"> Loa <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Loa kéo"> Loa kéo <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Amply "> Amply <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Cassette/ đài bỏ túi  ">  Cassette <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Micro/ Micro không dây/ Micro có dây  "> Micro <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Đầu Karaoke/Đầu Karaoke chấm điểm thông minh  "> Đầu Karaoke <i></i></a>
																	</li>
																	
																</ul>
																<ul class="dropdown_menu_child">
																	<li><strong>Tiện ích</strong></li>
																	<li><a href="" class="cat">Mua trả góp 0%</a></li>
																	<li><a href="" class="cat">Khuyến mãi</a></li>
																</ul>
															</nav>
														</div>
														<div class="dropdown_menu_img">
															<a href=""><img src="assets/img/hompage_05.png" alt=""></a>
														</div>
													</div>
												</div>
											</li>
											<li class="menu_child">
												<div class="menu_child-content">
													<a href="">Tivi</a>, <a href="">Loa thanh</a>, <a href="">Dàn âm thanh</a>
												</div>
											</li>
										</ul>
										<ul class="menu">
											<li class="menu_item">
												<a href="" class="menu_item_child">
													<i class="icon_dienlanh"></i>
													<span>Sản phẩm điện lạnh</span>
													<i class="arrow"></i>
												</a>
												<div class="dropdown_menu">
													<div class="dropdown_menu_parent">
														<div class="dropdown_menu_item">
															<nav >
																<ul class="dropdown_menu_child">
																	<li>
																		<a href="" class="cat">Tủ lạnh <i></i></a>
																		<nav class="trademark">
																			<ul>
																				<li><strong>THƯƠNG HIỆU</strong></li>
																				<li>
																					<a href="" class="cat_item">Sharp <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">AQUA <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Samsung <i></i></a>
																					<nav class="feature">
																						<ul>
																							<li><strong>TÍNH NĂNG</strong></li>
																							<li>
																								<a href="">Side by side</a>
																							</li>
																							<li>
																								<a href="">Inverter</a>
																							</li>
																							<li>
																								<a href="">Multi doors</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá trên</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá dưới</a>
																							</li>
																							<li>
																								<a href="">Mini</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																						<ul>
																							<li>
																								<strong>DUNG TÍCH</strong>
																							</li>
																							<li>
																								<a href="">Dưới 150L</a>
																							</li>
																							<li>
																								<a href="">150 - 300L</a>
																							</li>
																							<li>
																								<a href="">300 - 450L</a>
																							</li>
																							<li>
																								<a href="">Trên 450L</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																					</nav>
																				</li>
																				<li>
																					<a href="" class="cat_item">Pannasonic <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sony <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Toshiba <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Electrolux <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">LG <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Hitachi <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Alaska <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sanaky <i></i></a>
																				</li>
																				<div class="collapse">Thu gọn</div>
																				<div class="see_more">Xem thêm</div>
																			</ul>
																		</nav>
																	</li>
																	<li><a href="" class="cat">Tủ đông <i></i></a></li>
																	<li><a href="" class="cat">Tủ mát <i></i></a></li>
																	<li><a href="" class="cat">Máy lạnh <i></i></a></li>
																	<li><a href="" class="cat">Máy giặc <i></i></a></li>
																	<li><a href="" class="cat">Máy sấy <i></i></a></li>
																</ul>
																<ul class="dropdown_menu_child">
																	<li><strong>Tiện ích</strong></li>
																	<li><a href="" class="cat">Mua trả góp 0%</a></li>
																	<li><a href="" class="cat">Khuyến mãi</a></li>
																</ul>
															</nav>
														</div>
														<div class="dropdown_menu_img">
															<a href=""><img src="assets/img/hompage_05.png" alt=""></a>
														</div>
													</div>
												</div>
											</li>
											<li class="menu_child">
												<div class="menu_child-content">
													<a href="">Tủ lạnh</a>, <a href="">Máy lạnh</a>, <a href="">Máy giặt</a>
												</div>
												<div class="menu_child-content">
													<a href="">Tủ đông</a>, <a href="">Tủ mát</a>, <a href="">Máy sấy</a>
												</div>
												
											</li>
										</ul>
										<ul class="menu">
											<li class="menu_item">
												<a href="" class="menu_item_child">
													<i class="icon_didong"></i>
													<span>Sản phẩm di động</span>
													<i class="arrow"></i>
												</a>
												<div class="dropdown_menu">
													<div class="dropdown_menu_parent">
														<div class="dropdown_menu_item">
															<nav >
																<ul class="dropdown_menu_child">
																	<li>
																		<a href="" class="cat" title="Điện thoại/ Smartphone/ Samsung/ Apple/ Opppo/ Sony/ Vivo ">  Điện thoại - Smartphone <i></i></a>
																		<nav class="trademark">
																			<ul>
																				<li><strong>THƯƠNG HIỆU</strong></li>
																				<li>
																					<a href="" class="cat_item">Sharp <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">AQUA <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Samsung <i></i></a>
																					<nav class="feature">
																						<ul>
																							<li>
																								<strong>TÍNH NĂNG</strong>
																							</li>
																							<li>
																								<a href="">Side by side</a>
																							</li>
																							<li>
																								<a href="">Inverter</a>
																							</li>
																							<li>
																								<a href="">Multi doors</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá trên</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá dưới</a>
																							</li>
																							<li>
																								<a href="">Mini</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																						<ul>
																							<li>
																								<strong>DUNG TÍCH</strong>
																							</li>
																							<li>
																								<a href="">Dưới 150L</a>
																							</li>
																							<li>
																								<a href="">150 - 300L</a>
																							</li>
																							<li>
																								<a href="">300 - 450L</a>
																							</li>
																							<li>
																								<a href="">Trên 450L</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																					</nav>
																				</li>
																				<li>
																					<a href="" class="cat_item">Pannasonic <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sony <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Toshiba <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Electrolux <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">LG <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Hitachi <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Alaska <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sanaky <i></i></a>
																				</li>
																				<div class="collapse">Thu gọn</div>
																				<div class="see_more">Xem thêm</div>
																			</ul>
																		</nav>
																	</li>
																	<li><a href="" class="cat" title="Bao da/ tai nghe/ gậy selfie/ thẻ nhớ/ sạc dự phòng "> Phụ kiện di động<i></i></a></li>
																	<li><a href="" class="cat" title="Máy tính bảng/ Tablet ">Máy tính bảng <i></i></a></li>
																</ul>
																<ul class="dropdown_menu_child">
																	<li><strong>Tiện ích</strong></li>
																	<li><a href="" class="cat">Mua trả góp 0%</a></li>
																	<li><a href="" class="cat">Khuyến mãi</a></li>
																</ul>
															</nav>
														</div>
														<div class="dropdown_menu_img">
															<a href=""><img src="assets/img/hompage_05.png" alt=""></a>
														</div>
													</div>
												</div>
											</li>
											<li class="menu_child">
												<div class="menu_child-content">
													<a href="">Điện thoại</a>, <a href="">Tablet</a>, <a href="">Phụ kiện</a>
												</div>
											</li>
										</ul>
										<ul class="menu">
											<li class="menu_item">
												<a href="" class="menu_item_child">
													<i class="icon_giadung"></i>
													<span>Sản phẩm gia dụng</span>
													<i class="arrow"></i>
												</a>
												<div class="dropdown_menu">
													<div class="dropdown_menu_parent">
														<div class="dropdown_menu_item">
															<nav >
																<ul class="dropdown_menu_child">
																	<li>
																		<a href="" class="cat" title="Quạt điện/ Máy làm mát không khí/ Quạt phun sương/ Quạt điều hòa ">Quạt điện <i></i></a>
																		<nav class="trademark">
																			<ul>
																				<li><strong>THƯƠNG HIỆU</strong></li>
																				<li>
																					<a href="" class="cat_item">Sharp <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">AQUA <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Samsung <i></i></a>
																					<nav class="feature">
																						<ul>
																							<li>
																								<strong>TÍNH NĂNG</strong>
																							</li>
																							<li>
																								<a href="">Side by side</a>
																							</li>
																							<li>
																								<a href="">Inverter</a>
																							</li>
																							<li>
																								<a href="">Multi doors</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá trên</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá dưới</a>
																							</li>
																							<li>
																								<a href="">Mini</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																						<ul>
																							<li>
																								<strong>DUNG TÍCH</strong>
																							</li>
																							<li>
																								<a href="">Dưới 150L</a>
																							</li>
																							<li>
																								<a href="">150 - 300L</a>
																							</li>
																							<li>
																								<a href="">300 - 450L</a>
																							</li>
																							<li>
																								<a href="">Trên 450L</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																					</nav>
																				</li>
																				<li>
																					<a href="" class="cat_item">Pannasonic <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sony <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Toshiba <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Electrolux <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">LG <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Hitachi <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Alaska <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sanaky <i></i></a>
																				</li>
																				<div class="collapse">Thu gọn</div>
																				<div class="see_more">Xem thêm</div>
																			</ul>
																		</nav>
																	</li>
																	<li>
																		<a href="" class="cat" title="Máy làm mát (Quạt điều hòa) - Điện Máy Chợ Lớn"> Máy làm mát (Quạt điều hòa) <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Lò vi sóng - Lò nướng/ Máy làm bánh/ Bếp nướng điện ">Lò nướng <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="lò vi sóng, lò nướng giá rẻ chỉ có tại chợ lớn">Lò vi sóng <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Bếp Gas - Bếp âm / Bếp cồn/ Bếp gas dương/ Bếp gas mini"> Bếp Gas - Bếp âm  <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Bếp Từ - Bếp Hồng Ngoại / Bếp điện từ / Bếp hồng ngoại đôi / Bếp điện từ ">Bếp Từ - Bếp Hồng Ngoại <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Nồi áp suất/ Nồi hấp/ Nồi áp suất điện/ Nồi ủ nhiệt "> Nồi áp suất - hấp <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Nồi lẩu đa năng/ Nồi lẩu điện ">Nồi lẩu điện <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Máy xay sinh tố/ Máy xay thịt/ Máy đánh trứng ">Máy xay sinh tố <i></i></a>
																	</li>
																	
																</ul>
																<ul class="dropdown_menu_child">
																	<li><strong>Tiện ích</strong></li>
																	<li>
																		<a href="" class="cat">Mua trả góp 0%</a>
																	</li>
																	<li>
																		<a href="" class="cat">Khuyến mãi</a>
																	</li>
																</ul>
															</nav>
														</div>
														<div class="dropdown_menu_img">
															<a href=""><img src="assets/img/hompage_05.png" alt=""></a>
														</div>
													</div>
												</div>
											</li>
											<li class="menu_child">
												<div class="menu_child-content">
													<a href="">Nồi cơm</a>, <a href="">Quạt</a>, <a href="">Lò vi sóng</a>
												</div>
												<div class="menu_child-content">
													<a href="">Bếp gas</a>, <a href="">Bếp âm</a>, <a href=""> Bếp từ</a>...
												</div>
												<div class="menu_child-content">
													<a href="">Nồi</a>, <a href="">chảo</a>, <a href="">phụ kiện nhà bếp</a>
												</div>
											</li>
										</ul>
										<ul class="menu">
											<li class="menu_item">
												<a href="" class="menu_item_child">
													<i class="icon_noithat"></i>
													<span>Sản phẩm nội thất</span>
													<i class="arrow"></i>
												</a>
												<div class="dropdown_menu">
													<div class="dropdown_menu_parent">
														<div class="dropdown_menu_item">
															<nav >
																<ul class="dropdown_menu_child">
																	<li>
																		<a href="" class="cat" title="bàn/ tủ/ bàn làm việc/ ghế xoay/ ghế thư giãn "> Phòng khách<i></i></a>
																		<nav class="trademark">
																			<ul>
																				<li><strong>THƯƠNG HIỆU</strong></li>
																				<li>
																					<a href="" class="cat_item">Sharp <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">AQUA <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Samsung <i></i></a>
																					<nav class="feature">
																						<ul>
																							<li>
																								<strong>TÍNH NĂNG</strong>
																							</li>
																							<li>
																								<a href="">Side by side</a>
																							</li>
																							<li>
																								<a href="">Inverter</a>
																							</li>
																							<li>
																								<a href="">Multi doors</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá trên</a>
																							</li>
																							<li>
																								<a href="">Ngăn đá dưới</a>
																							</li>
																							<li>
																								<a href="">Mini</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																						<ul>
																							<li>
																								<strong>DUNG TÍCH</strong>
																							</li>
																							<li>
																								<a href="">Dưới 150L</a>
																							</li>
																							<li>
																								<a href="">150 - 300L</a>
																							</li>
																							<li>
																								<a href="">300 - 450L</a>
																							</li>
																							<li>
																								<a href="">Trên 450L</a>
																							</li>
																							<div class="collapse">Thu gọn</div>
																							<div class="see_more">Xem thêm</div>
																						</ul>
																					</nav>
																				</li>
																				<li>
																					<a href="" class="cat_item">Pannasonic <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sony <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Toshiba <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Electrolux <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">LG <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Hitachi <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Alaska <i></i></a>
																				</li>
																				<li>
																					<a href="" class="cat_item">Sanaky <i></i></a>
																				</li>
																				<div class="collapse">Thu gọn</div>
																				<div class="see_more">Xem thêm</div>
																			</ul>
																		</nav>
																	</li>
																	<li>
																		<a href="" class="cat" title="Bàn ăn/ ghế ăn "> Phòng ăn <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Bộ phòng ngủ/ giường/ tủ quần áo/ tab đầu giường/ bàn trang điểm/ nệm "> Phòng ngủ<i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="SOFA/ sofa thư giãn/ Sofa chữ L/ sofa giường "> Không gian khác <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Bộ sưu tập phòng khách, phòng ngủ, phòng ăn và không gia khác sang trọng đẹp mắt thư giản cho không gian nhà bạn">Bộ sưu tập <i></i></a>
																	</li>
																	<li>
																		<a href="" class="cat" title="Cây máng đồ/ ghế đôn/ ghế bàng "> Hàng trang trí <i></i></a>
																	</li>
																</ul>
																<ul class="dropdown_menu_child">
																	<li><strong>Tiện ích</strong></li>
																	<li>
																		<a href="" class="cat">Mua trả góp 0%</a>
																	</li>
																	<li>
																		<a href="" class="cat">Khuyến mãi</a>
																	</li>
																</ul>
															</nav>
														</div>
														<div class="dropdown_menu_img">
															<a href=""><img src="assets/img/hompage_05.png" alt=""></a>
														</div>
													</div>
												</div>
											</li>
											<li class="menu_child">
												<div class="menu_child-content">
													<a href="">Giường</a>, <a href="">Tủ</a>, <a href="">Bàn</a>
												</div>
												<div class="menu_child-content">
													<a href="">Sofa</a>, <a href="">nệm</a>, <a href="">trang trí nhà cửa</a>
												</div>
											</li>
										</ul>
										<ul class="menu">
											<li class="menu_item">
												<a href="" class="menu_item_child">
													<span class="tour">Tour du lịch và dịch vụ nạp tiền</span>
												</a>
											</li>
										</ul>
									</nav>
								</div>
							</div>
						</div>
					</div>
					<div class="list_promo">
						<a href="" class="promo_item">
							Mua trả góp 0%
						</a>
						<a href="" class="promo_item bg_red ">
							Giá sốc hôm nay
						</a>
						<a href="" class="promo_item promo_border">
							Khuyến mãi máy lạnh
						</a>
						<a href="" class="promo_item promo_border">
							Gia dụng giảm đến 49%
						</a>
						<a href="" class="promo_item">
							Nội thất khuyến mãi
						</a>
						
					</div>
				</div>
			</div>
		</div>
		
		<!-- content_page -->
		<div class="content_pro">
			<div class="max-width">
				<div class="content_pro-item">

					<div class="box_breadcrumb ">
						<nav class="box_breadcrumb-item">
							<ul>
								<li>
									<a href="">Trang chủ</a> /
								</li>
								<li>
									<a href="">Sản phẩm điện tử</a> /
								</li>
								<li>
									<a href="">Tivi</a>/
								</li>
								<li>
									<a href=""><strong>Tivi Samsung</strong></a>
								</li>
							</ul>
						</nav>
					</div>
					<div class="box_product">
						<div class="box_pro-images">
							<div class="box_pro-images-big owl-carousel" id="sync3">
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
								</a>
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_02.png" alt="ảnh Samsung Smart Tivi">
								</a>
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_03.png" alt="ảnh Samsung Smart Tivi">
								</a>
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
								</a>
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
								</a>
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_06.png" alt="ảnh Samsung Smart Tivi">
								</a>
								<a href="" class="item">
									<img src="assets/img/detail-product/detail_product_06_01.png" alt="ảnh Samsung Smart Tivi">
								</a>
								
							</div>
							<div class="box_pro-images-small-parent">
								<div class="box_pro-images-small owl-carouse" id="sync4">
									<div class="item">
										<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
									</div>
									<div class="item">
										<img src="assets/img/detail-product/detail_product_02.png" alt="ảnh Samsung Smart Tivi">
									</div>
									<div class="item">
										<img src="assets/img/detail-product/detail_product_03.png" alt="ảnh Samsung Smart Tivi">
									</div>
									<div class="item">
										<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
									</div>
									<div class="item">
										<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
									</div>
									<div class="item">
										<img src="assets/img/detail-product/detail_product_06.png" alt="ảnh Samsung Smart Tivi">
									</div>
									<div class="item">
										<img src="assets/img/detail-product/detail_product_06_01.png" alt="ảnh Samsung Smart Tivi">
									</div>
									
								</div>
							</div>
							<div class="see_zoom">
								<div class="see_zoom-img">
									<i class="icon_search_plus"></i>
									<span>Xem hình phóng to</span>
								</div>
								<div class="see_zoom-video">
									<i class="icon_video"></i>
									<span>Xem video giới thiệu</span>
								</div>
							</div>
							<div class="feature_pro">
								<div class="feature_item">
									<h3 class="title_feature" >Tính năng nổi bật: </h3>
									<p>- Kích thước màn hình: 50 inch</p>
									<p>- Độ phân giải: Full HD 1920x1080</p>
									<p>- Hệ điều hành, giao diện: Linux</p>
									<p>- Kích thước màn hình: 50 inch</p>
									<p>- Độ phân giải: Full HD 1920x1080</p>
									<p>- Hệ điều hành, giao diện: Linux</p>
									<p>- Kích thước màn hình: 50 inch</p>
									<p>- Độ phân giải: Full HD 1920x1080</p>
									<p>- Hệ điều hành, giao diện: Linux</p>
									<p>- Kích thước màn hình: 50 inch</p>
									<p>- Độ phân giải: Full HD 1920x1080</p>
									<p>- Hệ điều hành, giao diện: Linux</p>
								</div>
								<div class="feature_see_more">Xem thêm tính năng<i class="arrow_down"></i></div>
							</div>
						</div>
						<div class="box_pro-info">
							<div class="name_pro_detail">
								<h3>Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300</h3>
							</div>
							<div class="trademark_detail">
								<p>Thương hiệu: <a href="" title="">Samsung</a> </p>
							</div>
							<div class="sku_detail">
								<p>Mã SKU: <span>1247300</span></p>
							</div>
							<div class="evalute_detail">
								<div class="my-rating" data-rating="4.5"></div>
								<a href="">Có 12 đánh giá</a>
							</div>

							<!-- <div class="like_share">
								<div class="info_review"> <div id="fb-root"></div>
								<script> var only_run=true; setTimeout(function(){ if(only_run){ only_run=false; (function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=1891604161102448"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'facebook-jssdk')); } },1e4); </script>
							</div> -->
							<div class="sticker">
								<a href=""><img src="assets/img/detail-product/detail_product_07.png" alt="ảnh sticker"></a>
							</div>
							<div class="size_pro">
								<h3 class="title_feature" >Kích cỡ: </h3>
								<p>Có <strong>4 kích cỡ màn hình.</strong> Bạn đang chọn <strong>43 inch</strong></p>
								<a href="" class="adress_stock"><i></i> Xem 9 siêu thị còn hàng</a>
							</div>

							<div class="many_size_pro">
								<div class="one_size">
									<label class="container-detail">
										<span class="size-pro">43 inch</span>
										<span class="price-pro">9.350.000Đ</span>
										<input type="radio"  name="radio">
										<span class="checkmark"></span>
									</label>

								</div>	
								<div class="one_size active">
									<label class="container-detail">
										<span class="size-pro">49 inch</span>
										<span class="price-pro">16.950.000Đ</span>
										<input type="radio" checked="checked" name="radio">
										<span class="checkmark"></span>
									</label>
								</div>
								<div class="one_size">
									<label class="container-detail">
										<span class="size-pro">55 inch</span>
										<span class="price-pro">24.450.000Đ</span>
										<input type="radio"  name="radio">
										<span class="checkmark"></span>
									</label>

								</div>	
								<div class="one_size">
									<label class="container-detail">
										<span class="size-pro">57 inch</span>
										<span class="price-pro">28.950.000Đ</span>
										<input type="radio"  name="radio">
										<span class="checkmark"></span>
									</label>
								</div>

							</div>

							<div class="gift_pro">
								<h3><i class="icon_gift_pro"></i> Quà tặng kèm trị giá 10.500.000đ</h3>
								<div class="gift_item">
									<ul>
										<li>Giảm Thêm Tiền <strong>600.000đ</strong></li>
										<li>Tặng Nồi Chiên Lock&Lock (Hết Quà Trừ <strong>700.000đ</strong>)</li>
										<li>Giảm 1 triệu khi mua bộ nồi Elmich EL-8668</li>
										<li>Mua Loa Samsung Q60R  Với Giá <strong>4.990.000đ</strong> (Giá chính hãng <strong>9.990.000đ</strong>)</li>
										<li>Tặng Combo Dịch Vụ & Phiếu Mua Hàng <strong>700.000đ</strong> (800140)</li>
									</ul>
								</div>
							</div>

							<div class="buy_pro">
								<button type="button" class="click_buy">MUA NGAY <span>Giao hàng tận nơi</span></button>
								<button type="button" class="buy_installment">MUA TRẢ GÓP <span>Chỉ có 2.066.667đ/tháng (6 tháng)</span></button>
							</div>
						</div>
						<div class="box_pro-benefit">
							<div class="monopoly">
								<h3> Độc quyền tại Điện máy Chợ Lớn</h3>
								<div class="monopoly_item">
									<ul>
										<li>
											<i class="icon_genuine"></i>
											<div class="monopoly-title">
												<p>Hàng chính hãng <strong>100%</strong></p>
											</div>
										</li>
										<!-- <li>
											<i class="icon_change"></i>
											<div class="monopoly-title">
												<p><strong>1</strong> đổi <strong>1</strong> trong <strong>35</strong> ngày <a href="">(Chính sách)</a></p>
												
											</div>

										</li>
										<li>
											<i class="icon_refund"></i>
											<div class="monopoly-title">
												<p>Hoàn tiền <strong>x2</strong> nếu nơi khác bán rẻ hơn. <a href="">(Chính sách)</a></p>
												
											</div>
										</li> -->
										<li>
											<i class="icon_guarantee_mb"></i>
											<div class="monopoly-title">
												<p>Bảo hành chính hãng tới <strong>2+ 1 năm</strong> <a href="">(Chính sách)</a></p>

											</div>
										</li>
										<li>
											<i class="icon_delivery"></i>
											<div class="monopoly-title">
												<p>Giao hàng miễn phí tận nơi <a href="">(Xem chi tiết)</a></p>
												
											</div>

										</li>
										<li>
											<i class="icon_installment"></i>
											<div class="monopoly-title">
												<p>Trả góp 0% lãi suất <a href="">(Chính sách)</a></p>
												
											</div>
										</li>

									</ul>
								</div>
							</div>
							<div class="evaluate_product">
								<h3>Tại sao bạn nên mua sản phẩm này?</h3>
								<div class="evaluate_item">
									<p>Các chuyên gia đã đánh giá</p>
									<div class="sum_evaluate">Xuất sắc 8.7/10</div>
									<ul>
										<li>
											<span class="title_eva">Sản phẩm chất lượng</span>
											<span class="point_eva">9.0</span>
										</li>
										<li>
											<span class="title_eva">Đáng đồng tiền</span>
											<span class="point_eva">10.0</span>
										</li>
										<li>
											<span class="title_eva">Hữu dụng</span>
											<span class="point_eva">7.0</span>
										</li>
										<li>
											<span class="title_eva">Tiện ích cao</span>
											<span class="point_eva">8.5</span>
										</li>
										
									</ul>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
			<div class="react" id= "root"></div>
			<div class="max-width">
				<h3 class="title_product">Sản phẩm cùng phân khúc giá</h3>
				<div class="compare_pro_other">
					<label class="container_checkbox">So sánh với sản phẩm Tivi khác
						<input type="checkbox" >
						<span class="checkmark_checkbox"></span>
					</label>
				</div>
				
				<div class="slide_product owl-carousel owl-theme" id="slide_pro">
					
					<div class="product">
						<div class="lable_pro">
							<p class="icon_selling">Bán chạy <span>#01</span></p>
							<p class="icon_buy">Nên mua</p>
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/detail-product/detail_product_13.png" alt="">
							</a>
						</div>
						<div class="pro_promo">
							<div class="icon_flash">
								<p>Siêu sale ngày rực</p>
							</div>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png " alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>9.350.000đ</span>
						</div>
						<div class="gift">
							<span>Giảm 500.000đ và Quà 150.000đ</span>
						</div>
						<div class="gift_detail">

						</div>
						<div class="evaluate_compare">
							<span class="evaluate"><i></i>Đánh giá 8.7/10</span>
							<a href="" class="compare">So sánh</a>
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
							<!-- <p class="icon_selling">Bán chạy <span>#01</span></p>
							<p class="icon_buy">Nên mua</p> -->
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/detail-product/detail_product_14.png" alt="">
							</a>
						</div>
						<div class="pro_promo">
							<div class="icon_flash">
								<p>Siêu sale ngày rực</p>
							</div>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png " alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>19.590.000đ</span>
						</div>
						<div class="discount">
							<span class="price_market">24.590.000đ</span>
							<span class="discount_percent">-35%</span>
							<span class="installment">Trả góp 0%</span>
						</div>
						<div class="gift_detail">
						</div>
						<div class="evaluate_compare">
							<span class="evaluate"><i></i>Đánh giá 8.7/10</span>
							<a href="" class="compare">So sánh</a>
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
							<!-- <p class="icon_selling">Bán chạy <span>#01</span></p> -->
							<p class="icon_buy">Nên mua</p>
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/detail-product/detail_product_15.png" alt="">
							</a>
						</div>
						<div class="pro_promo">
							<div class="icon_flash">
								<p>Siêu sale ngày rực</p>
							</div>
 						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png " alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>32.490.000đ</span>
						</div>
						<div class="discount">
							<span class="price_market">24.590.000đ</span>
							<span class="discount_percent">-15%</span>
						</div>
						<div class="gift_detail">
							<span>Tặng máy pha cà phê</span>
						</div>
						<div class="evaluate_compare">
							<span class="evaluate"><i></i>Đánh giá 8.7/10</span>
							<a href="" class="compare">So sánh</a>
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
							<p class="icon_selling">Bán chạy <span>#01</span></p>
							<!-- <p class="icon_buy">Nên mua</p> -->
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/detail-product/detail_product_16.png" alt="">
							</a>
						</div>
						<div class="pro_promo">
							<div class="icon_flash">
								<p>Siêu sale ngày rực</p>
							</div>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png " alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>19.590.000đ</span>
						</div>
						<div class="gift">
							<span>Giảm 1.000.000đ </span>
						</div>
						<div class="gift_detail">
							
						</div>
						<div class="evaluate_compare">
							<span class="evaluate"><i></i>Đánh giá 8.7/10</span>
							<a href="" class="compare">So sánh</a>
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
							<!-- <p class="icon_selling">Bán chạy <span>#01</span></p> -->
							<p class="icon_buy">Nên mua</p>
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/detail-product/detail_product_17.png" alt="">
							</a>
						</div>
						<div class="pro_promo">
							<div class="icon_flash">
								<p>Siêu sale ngày rực</p>
							</div>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png " alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>9.350.000đ</span>
						</div>
						<div class="gift">
							<span>Giảm 1.000.000đ </span>
						</div>
						<div class="gift_detail">
						</div>
						<div class="evaluate_compare">
							<span class="evaluate"><i></i>Đánh giá 8.7/10</span>
							<a href="" class="compare">So sánh</a>
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
							<p class="icon_selling">Bán chạy <span>#01</span></p>
							<p class="icon_buy">Nên mua</p>
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/detail-product/detail_product_15.png" alt="">
							</a>
						</div>
						<div class="pro_promo">
							<div class="icon_flash">
								<p>Siêu sale ngày rực</p>
							</div>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png " alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>9.350.000đ</span>
						</div>
						<div class="gift">
							<span>Giảm 1.000.000đ </span>
						</div>
						<div class="gift_detail">
						</div>
						<div class="evaluate_compare">
							<span class="evaluate"><i></i>Đánh giá 8.7/10</span>
							<a href="" class="compare">So sánh</a>
						</div>
					</div>
					
				</div>
			</div>

			<div class="max-width">
				<div class="content_pro-item">
					<nav class="info_pro-title">
						<ul>
							<li class="active">Thông số kỹ thuật</li>
							<li >Mô tả sản phẩm</li>
							<li >Hướng dẫn sử dụng</li>
						</ul>
					</nav>
					
					<div class="info_pro-tab">
						<div class="info_pro-item current">
							<div class="info_pro-child">
								<div class="img_detail_pro">
									<img src="assets/img/detail-product/detail_product_18.png" alt="" >
								</div>
								<div class="detail_specifications">
									<nav class="list_specifications">
										<ul>
											<li class="title-specification">
												<strong>Màn hình</strong>
											</li>
											<li>
												<p>Công nghệ màn hình:</p>
												<p><strong>Super AMOLED</strong></p>
											</li>
											<li>
												<p>Độ phân giải:</p>
												<p>Ultra 4K</p>
											</li>
											<li>
												<p>Màn hình rộng:</p>
												<p>49 inch</p>
											</li>
											<li>
												<p>Cảm ứng:</p>
												<p><strong>Cảm ứng điện dung đa điểm</strong></p>
											</li>
											<li>
												<p>Mặt kính cảm ứng:</p>
												<p>Kính cường lực Gorilla Glass 4</p>
											</li>
											<li class="title-specification">
												<strong>3D</strong>
											</li>
											<li>
												<p>Xem 3D:</p>
												<p>Không</p>
											</li>
											<li class="title-specification">
												<strong>Âm thanh</strong>
											</li>
											<li>
												<p>Tổng Công Suất Loa:</p>
												<p><strong>20W</strong></p>
											</li>
											<li>
												<p>Độ phân giải:</p>
												<p>1440 x 2560 pixels</p>
											</li>
											<li>
												<p>Độ phân giải:</p>
												<p>1440 x 2560 pixels</p>
											</li>
											<li>
												<p>Độ phân giải:</p>
												<p>1440 x 2560 pixels</p>
											</li>
											<li>
												<p>Độ phân giải:</p>
												<p>1440 x 2560 pixels</p>
											</li>
											<li>
												<p>Độ phân giải:</p>
												<p>1440 x 2560 pixels</p>
											</li>
											
										</ul>
									</nav>
									<div class="see_info"><p class="see_specifications">Xem thêm thông số kỹ thuật <i></i></p></div>
								</div>
							</div>
							
						</div>
						<div class="info_pro-item">
							<div class="des_pro">
								<div class="des_pro_item">
									<p><strong>Tivi màn hình OLED đẳng cấp</strong></p>
									<p>Android Tivi OLED Sony 4K 65 inch KD-65A8G thu hút ánh nhìn không chỉ bởi vẻ ngoài hiện đại, sang trọng mà còn bởi khả hiển thị hình ảnh cực nét, mang đến người dùng trải nghiệm giải trí đã mắt ngay tại nhà.</p>
									<br>
									<p>Tivi Sony 65 inch có kích thước màn hình lớn đi cùng viền màn hình siêu mỏng (0.4 cm) (bằng một nửa iPhone 11 Pro Max 0.8 cm) sẽ là lựa chọn hàng đầu ở những không gian rộng lớn: phòng họp, phòng hội nghị hay có thể là phòng chiếu phim tại gia.</p>
									<img src="assets/img/detail-product/detail_product_20.jpg" alt="">
									<p><strong>Tái hiện sắc đen ấn tượng với công nghệ màn hình OLED</strong>
									</p>
									<p>OLED tivi Sony nhờ được trang bị công nghệ này không chỉ có thể tái hiện sắc đen hoàn hảo mà những màu sắc khác cũng đều rực rỡ, bắt mắt và trung thực.</p>
									<img src="assets/img/detail-product/detail_product_19.jpg" alt="">
									<p>OLED tivi Sony nhờ được trang bị công nghệ này không chỉ có thể tái hiện sắc đen hoàn hảo mà những màu sắc khác cũng đều rực rỡ, bắt mắt và trung thực.</p>

									<p><strong>Tái hiện sắc đen ấn tượng với công nghệ màn hình OLED</strong>
									</p>
									<p>OLED tivi Sony nhờ được trang bị công nghệ này không chỉ có thể tái hiện sắc đen hoàn hảo mà những màu sắc khác cũng đều rực rỡ, bắt mắt và trung thực.</p>
									<img src="assets/img/detail-product/detail_product_21.jpg" alt="">
									<p>OLED tivi Sony nhờ được trang bị công nghệ này không chỉ có thể tái hiện sắc đen hoàn hảo mà những màu sắc khác cũng đều rực rỡ, bắt mắt và trung thực.</p>
								</div>

							</div>
							<div class="see_info "><p class="see_feature">Xem thêm tính năng sản phẩm <i></i></p></div>
						</div>
						<div class="info_pro-item">
							<div class="videos_active">
								<div class="many_video">
									<div class="video_banner">
										<iframe class="video_iframe" width="500" height="300" src="https://www.youtube.com/embed/5YmhGFvsQ9o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
										<h3>Hướng dẫn sử dụng và đánh giá về Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300</h3>
										<p>30/12/2019</p>
									</div>
									<div class="videos_small">
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/SyQNuf4lKaI"><img src="assets/img/detail-product/detail_product_22.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/SyQNuf4lKaI">Hướng dẫn chỉnh âm thanh trên Tivi Smart Samsung để hát karaoke</a>
													<span>29/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/4z7IlWHCuJU"><img src="assets/img/detail-product/detail_product_23.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/4z7IlWHCuJU">Video review tính năng tìm giọng nói thông minh Tivi Smart...</a>
													<span>28/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/ic-VsdtLGj4"><img src="assets/img/detail-product/detail_product_24.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/ic-VsdtLGj4">Hướng dẫn chỉnh âm thanh trên Tivi Smart Samsung hiệu quả nhất</a>
													<span>27/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0"><img src="assets/img/detail-product/detail_product_25.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0">Hướng dẫn cài đặt tính năng kỹ thuật số trên Tivi Smart Samsung...</a>
													<span>26/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0"><img src="assets/img/detail-product/detail_product_22.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href=" https://www.youtube.com/embed/7odBl90Azi0">Hướng dẫn chỉnh âm thanh trên Tivi Smart Samsung để hát...</a>
													<span>25/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0"><img src="assets/img/detail-product/detail_product_22.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0">Hướng dẫn chỉnh âm thanh trên Tivi Smart Samsung để hát...</a>
													<span>24/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0"><img src="assets/img/detail-product/detail_product_22.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0">Hướng dẫn chỉnh âm thanh trên Tivi Smart Samsung để hát...</a>
													<span>23/12/2019</span>
												</div>
											</div>
										</div>
										<div class="v_small">
											<div class="v_small_item">
												<div class="v_img_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0"><img src="assets/img/detail-product/detail_product_22.png" alt=""></a>
												</div>
												<div class="info_video">
													<a href="https://www.youtube.com/embed/7odBl90Azi0">Hướng dẫn chỉnh âm thanh trên Tivi Smart Samsung để hát...</a>
													<span>22/12/2019</span>
												</div>
											</div>
										</div>
										<div class="see_info see_add_video"><p class="see_videos">Xem thêm video khác <i></i></p></div>
									</div>
								</div>

								
							</div>
							<div class="videos_none hide">
								<img src="assets/img/detail-product/detail_product_26.png" alt="">
								<p><strong>Hiện tại chưa có videos</strong></p>
								<p>Chúng tôi đang cập nhật trong thời gian sớm nhất.</p>
							</div>
						</div>
						
					</div>
				</div>
			</div>

			<div class="max-width">
				<div class="content_pro-item">
					<div class="content_eva_exp">
						<div class="content_evaluate">
							<h3 class="title_evaluate">Đánh giá sản phẩm</h3>
							<div class="star_box">
								<div class="star_average">
									<h3>4.9 / 5</h3>
									<div class="ratingresult">
										<span><i class="iconcom_star"></i></span>
										<span><i class="iconcom_star"></i></span>
										<span><i class="iconcom_star"></i></span>
										<span><i class="iconcom_star"></i></span>
										<span><i class="iconcom_halfstar"></i></span>
										<span><strong>199</strong> đánh giá</span>
									</div>
								</div>
								<div class="star_lines">
									<div class="star_line">
										<div class="star_type">
											<strong>5</strong>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
										</div>
										<div class="star_bar" data-percent="55%">
											<div class="star_barsub"></div>
										</div>
										<div class="star_sum_eva">
											<span>101</span>
										</div>
									</div>
									<div class="star_line">
										<div class="star_type">
											<strong>4</strong>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
										</div>
										<div class="star_bar" data-percent="45%">
											<div class="star_barsub"></div>
										</div>
										<div class="star_sum_eva">
											<span>95</span>
										</div>
									</div>
									<div class="star_line">
										<div class="star_type">
											<strong>3</strong>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
										</div>
										<div class="star_bar" data-percent="28%">
											<div class="star_barsub"></div>
										</div>
										<div class="star_sum_eva">
											<span>3</span>
										</div>
									</div>
									<div class="star_line">
										<div class="star_type">
											<strong>2</strong>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
										</div>
										<div class="star_bar" data-percent="0%">
											<div class="star_barsub"></div>
										</div>
										<div class="star_sum_eva">
											<span>0</span>
										</div>
									</div>
									<div class="star_line">
										<div class="star_type">
											<strong>1</strong>
											<span><i class="iconcom_star revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
											<span><i class="iconcom_unstar revert"></i></span>
										</div>
										<div class="star_bar" data-percent="0%">
											<div class="star_barsub"></div>
										</div>
										<div class="star_sum_eva">
											<span>0</span>
										</div>
									</div>
								</div>
							</div>
							<div class="rate_now">
								<p><i></i>Đánh giá ngay</p>
							</div>
							<div class="criteria_eva">
								<p>Tiêu chí đánh giá</p>
								<ul>
									<li>Sản phẩm có chất lượng không?</li>
									<li>Sản phẩm có đáng đồng tiền không?</li>
									<li>Âm thanh có hay và sống động không?</li>
									<li>Hướng dẫn sử dụng có hữu dụng hay không?</li>
									<li>Tiện ích dễ sử dụng không?</li>
								</ul>
							</div>
							<div class="comment_product">
								<p>Nhận xét về sản phẩm</p>
								<div class="sort_filter">
									<div class="sort">
										<span><i></i>Sắp xếp theo:</span>
										<div class="slect_sort">
											<select id="sort">
												<option value="new" selected="selected">Mới nhất</option>
												<option value="all">Tất cả</option>
											</select>
										</div>

									</div>
									<div class="filter">
										<span><i></i>Lọc: </span>
										<div class="select_filter">
											<select id="filter">
												<option value="allstar" selected="selected">Tất cả sao</option>
												<option value="star5">5 sao</option>
												<option value="star4">4 sao</option>
												<option value="star3">3 sao</option>
												<option value="star2">2 sao</option>
												<option value="star1">1 sao</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							
							<div class="img_customer">
								<h3 class="title_evaluate">Hình ảnh từ khách hàng</h3>
								<div class="list_img_cus">
									<div class="img_cus owl-carousel owl-theme" id="img_cus">
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_27.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_28.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_29.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_30.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_31.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_32.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_30.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_29.png" alt="">
										</a>
										<a href="" class="item">
											<img src="assets/img/detail-product/detail_product_28.png" alt="">
										</a>

									</div>
								</div>
							</div>

							<div class="infocomment">
								<div class="comment_ask">
									<div class="comment_ask-user">
										<i class="icon_user"></i>
										<strong>Hoàng Phúc</strong>
										<span class="user_member">Diamond Member</span>
									</div>
									<div class="star-rate">
										<div class="star_user">
											<div class="my-rating" data-rating="4.5"></div>
										</div>
										<div class="tab_rate">
											<span>Sản phẩm chất lượng</span>
											<span>Đáng đồng tiền</span>
											<span>Âm thanh hay và sống động</span>
										</div>
									</div>	
									<div class="info_ask">Vừa mua hôm qua cảm nhận mọi thứ ban đầu rất là ok. Ko biết dùng về sau thế nào. Nhưng trc mắt thấy hình ảnh đẹp. Âm thanh to nghe êm. Vào mạng và tìm kiếm giọng nói rất nhạy và chính xác.</div>
									<div class="relate_info">
										<span class="comment">Bình luận</span>
										<span class="like"><i class="icon_like"></i>Thích</span>
										<span class="dislike"><i class="icon_dislike"></i>Không thích</span>
										<span class="status">2 giờ 17 phút trước</span>
									</div>
									<div class="comment_active">
										<form action="">
											<div class="comment_customer">
												<textarea id="" rows="1" cols="42" name="comment_customer" placeholder="Nhập bình luận của bạn..."  ></textarea>
											</div>
											<button type="button" class="cancel">Hủy</button>
											<button type="submit" class="sent">Gửi</button>
										</form>
									</div>
								</div>

								<div class="comment_ask">
									<div class="comment_ask-user">
										<i class="icon_user"></i>
										<strong>Tu Nguyen</strong>
										<span class="user_member">Gold Member</span>
									</div>	
									<div class="star-rate">
										<div class="star_user">
											<div class="my-rating" data-rating="4.5"></div>
										</div>
										<div class="tab_rate">
											<span>Sản phẩm chất lượng</span>
											<span>Đáng đồng tiền</span>
											<span>Hữu dụng</span>
											<span>Âm thanh hay và sống động</span>
											<span>Dễ sử dụng</span>
										</div>
									</div>	
									<div class="info_ask">Ti vi màu đẹp, nét , âm thanh rõ. Mẫu tivi mỏng, sang trọng, bắt wifi mạnh và nhanh. Sản phẩm tốt màn hình sắc nét. Âm thành to rõ.</div>
									<div class="images_ask">
										<img src="assets/img/detail-product/detail_product_31.png" alt="">
										<img src="assets/img/detail-product/detail_product_27.png" alt="">
										<img src="assets/img/detail-product/detail_product_32.png" alt="">
									</div>
									<div class="relate_info">
										<span class="comment">8 Bình luận</span>
										<span class="like"><i class="icon_like active"></i>1.9k Thích</span>
										<span class="dislike"><i class="icon_dislike active"></i>3 Không thích</span>
										<span class="status">1 ngày trước</span>
									</div>
									<div class="comment_active">
										<form action="">
											<div class="comment_customer">
												<textarea id="" rows="1" cols="42" name="comment_customer" placeholder="Nhập bình luận của bạn..."  ></textarea>
											</div>
											<button type="button" class="cancel">Hủy</button>
											<button type="submit" class="sent">Gửi</button>
										</form>
									</div>
									<div class="comment_reply">
										<div class="comment_ask-user">
											<i class="icon_user"></i>
											<strong>Bin Bin</strong>
											<span class="user_member">Star Member</span>
										</div>	
										<div class="info_ask">Chắc mình phải gọi nhờ tư vấn kỹ hơn! Cảm ơn bạn đã chia sẻ thông tin.</div>
										<div class="relate_info">
											<span class="like"><i class="icon_like"></i>59 Thích</span>
											<span class="dislike"><i class="icon_dislike"></i>1 Không thích</span>
											<span class="status">21 giờ 8 phút...</span>
										</div>
									</div>
									
								</div>
								<div class="comment_ask">
									<div class="comment_ask-user">
										<i class="icon_user"></i>
										<strong>Tu Nguyen</strong>
										<span class="user_member">Gold Member</span>
									</div>	
									<div class="star-rate">
										<div class="star_user">
											<div class="my-rating" data-rating="4.5"></div>
										</div>
										<div class="tab_rate">
											<span>Sản phẩm chất lượng</span>
											<span>Đáng đồng tiền</span>
											<span>Hữu dụng</span>
											<span>Âm thanh hay và sống động</span>
											<span>Dễ sử dụng</span>
										</div>
									</div>	
									<div class="info_ask">Ti vi màu đẹp, nét , âm thanh rõ. Mẫu tivi mỏng, sang trọng, bắt wifi mạnh và nhanh. Sản phẩm tốt màn hình sắc nét. Âm thành to rõ.</div>
									<div class="images_ask">
										<img src="assets/img/detail-product/detail_product_31.png" alt="">
										<img src="assets/img/detail-product/detail_product_27.png" alt="">
										<img src="assets/img/detail-product/detail_product_22.png" alt="">
									</div>
									<div class="relate_info">
										<span class="comment">8 Bình luận</span>
										<span class="like"><i class="icon_like"></i>1.9k Thích</span>
										<span class="dislike"><i class="icon_dislike"></i>3 Không thích</span>
										<span class="status">1 ngày trước</span>
									</div>
									<div class="comment_active">
										<form action="">
											<div class="comment_customer">
												<textarea id="" rows="1" cols="42" name="comment_customer" placeholder="Nhập bình luận của bạn..."  ></textarea>
											</div>
											<button type="button" class="cancel">Hủy</button>
											<button type="submit" class="sent">Gửi</button>
										</form>
									</div>
									<div class="comment_reply">
										<div class="comment_ask-user">
											<i class="icon_user"></i>
											<strong>Bin Bin</strong>
											<span class="user_member">Star Member</span>
										</div>	
										<div class="info_ask">Chắc mình phải gọi nhờ tư vấn kỹ hơn! Cảm ơn bạn đã chia sẻ thông tin.</div>
										<div class="relate_info">
											<span class="like"><i class="icon_like"></i>59 Thích</span>
											<span class="dislike"><i class="icon_dislike"></i>1 Không thích</span>
											<span class="status">21 giờ 8 phút...</span>
										</div>
									</div>
									
								</div>
								<div class="comment_ask">
									<div class="comment_ask-user">
										<i class="icon_user"></i>
										<strong>Tu Nguyen</strong>
										<span class="user_member">Gold Member</span>
									</div>	
									<div class="star-rate">
										<div class="star_user">
											<div class="my-rating" data-rating="4.5"></div>
										</div>
										<div class="tab_rate">
											<span>Sản phẩm chất lượng</span>
											<span>Đáng đồng tiền</span>
											<span>Hữu dụng</span>
											<span>Âm thanh hay và sống động</span>
											<span>Dễ sử dụng</span>
										</div>
									</div>	
									<div class="info_ask">Ti vi màu đẹp, nét , âm thanh rõ. Mẫu tivi mỏng, sang trọng, bắt wifi mạnh và nhanh. Sản phẩm tốt màn hình sắc nét. Âm thành to rõ.</div>
									<div class="images_ask">
										<img src="assets/img/detail-product/detail_product_31.png" alt="">
										<img src="assets/img/detail-product/detail_product_27.png" alt="">
										<img src="assets/img/detail-product/detail_product_32.png" alt="">
									</div>
									<div class="relate_info">
										<span class="comment">8 Bình luận</span>
										<span class="like"><i class="icon_like"></i>1.9k Thích</span>
										<span class="dislike"><i class="icon_dislike"></i>3 Không thích</span>
										<span class="status">1 ngày trước</span>
									</div>
									<div class="comment_active">
										<form action="">
											<div class="comment_customer">
												<textarea id="" rows="1" cols="42" name="comment_customer" placeholder="Nhập bình luận của bạn..."  ></textarea>
											</div>
											<button type="button" class="cancel">Hủy</button>
											<button type="submit" class="sent">Gửi</button>
										</form>
									</div>
									<div class="comment_reply">
										<div class="comment_ask-user">
											<i class="icon_user"></i>
											<strong>Bin Bin</strong>
											<span class="user_member">Star Member</span>
										</div>	
										<div class="info_ask">Chắc mình phải gọi nhờ tư vấn kỹ hơn! Cảm ơn bạn đã chia sẻ thông tin.</div>
										<div class="relate_info">
											<span class="like"><i class="icon_like"></i>59 Thích</span>
											<span class="dislike"><i class="icon_dislike"></i>1 Không thích</span>
											<span class="status">21 giờ 8 phút...</span>
										</div>
									</div>
									
								</div>
								<div class="see_more_comment">
									<p>Xem thêm bình luận <i class="arrow_down"></i></p>
								</div>
								<div class="pages">
									<ul>
										<li>1</li>
										<li class="active">2</li>
										<li>3</li>
										<li>4</li>
										<li>5</li>
										<li>6</li>
										<li>...</li>
										<li>9</li>
									</ul>
								</div>
								
							</div>
							<div class="ask_reply_pro">
								<h3 class="title_evaluate">Hỏi, đáp về sản phẩm</h3>
								<div class="ask_pro">
									<form action="">
										<div class="comment_customer">
											<textarea id="" rows="1" cols="49" name="ask_product"  placeholder="Hãy đặt câu hỏi liên quan đến sản phẩm"  ></textarea>
										</div>
										<button type="button" class="cancel">Hủy</button>
										<button type="submit" class="sent">Gửi</button>
									</form>
								</div>
							</div>

							<div class="comment_ask border-top-0">
								<i class="icon_user"></i>
								<strong>Khách hàng mới</strong>
								<div class="info_ask">Tôi muốn mua tivi này nhưng không biết có mua trả góp được không?</div>
								<div class="relate_info">
									<span class="comment">Trả lời</span>
									<span class="like"><i class="icon_like"></i>Thích</span>
									<span class="status">21 giờ 8 phút 14 giây trước</span>
								</div>
								<div class="comment_active">
									<form action="">
										<div class="comment_customer">
											<textarea id="" rows="1" cols="43" name="comment_customer"  placeholder="Nhập bình luận của bạn..."  ></textarea>
										</div>
										<button type="button" class="cancel">Hủy</button>
										<button type="submit" class="sent">Gửi</button>
									</form>
								</div>
								<div class="comment_reply">
									<i class="icon_user"></i>
									<strong>Phương Khánh</strong>
									<span class="administrators">Quản trị viên</span>
									<div class="info_ask">Chào bạn,<br>
										Sản phẩm này hiện có hỗ trợ trả góp online ạ. Bạn có thể tới trực tiếp chi nhánh tham khảo và được hỗ trợ trả góp qua công ty tài chính nhé. Nếu cần thêm thông tin sản phẩm, bạn vui lòng để lại số điện thoại bên mình sẽ liên hệ tư vấn cho bạn hoặc bạn có thể liên hệ số 028-3856 3388 để được hỗ trợ nhé.<br>
									Thân chào!</div>
									<div class="relate_info">
										<span class="comment">Trả lời</span>
										<span class="like"><i class="icon_like"></i>Hài lòng</span>
										<span class="dislike"><i class="icon_dislike"></i>Không hài lòng</span>
										<span class="status">21 giờ 8 phút...</span>
									</div>

									<div class="comment_active comment_active_item">
										<form action="">
											<div class="comment_customer">
												<textarea id="" rows="1" cols="38" name="comment_customer" placeholder="Nhập bình luận của bạn..."  ></textarea>
											</div>
											<button type="button" class="cancel">Hủy</button>
											<button type="submit" class="sent">Gửi</button>
										</form>
									</div>
								</div>

							</div>
							<div class="comment_ask border-top-0">
								<i class="icon_user"></i>
								<strong>Trần Đức Minh</strong>
								<span class="user_member">Diamond Member</span>
								<div class="info_ask">Tôi có thẻ thành viên Diamond thì có được ưu đãi gì khi mua sản phẩm này ko?</div>
								<div class="relate_info">
									<span class="comment">Trả lời</span>
									<span class="like"><i class="icon_like"></i>Thích</span>
									<span class="status">21 giờ 8 phút 14 giây trước</span>
								</div>
								<div class="comment_active">
									<form action="">
										<div class="comment_customer">
											<textarea id="" rows="1" cols="43" name="comment_customer"  placeholder="Nhập bình luận của bạn..."  ></textarea>
										</div>
										<button type="button" class="cancel">Hủy</button>
										<button type="submit" class="sent">Gửi</button>
									</form>
								</div>
								<div class="comment_reply">
									<i class="icon_user"></i>
									<strong>Ngọc Kiều</strong>
									<span class="administrators">Quản trị viên</span>
									<div class="info_ask">Chào bạn,<br>
										Bạn vui lòng để lại số điện thoại bên mình sẽ liên hệ tư vấn cho bạn hoặc bạn có thể liên hệ số 028-3856 3388 để được hỗ trợ nhé.<br>
									Thân chào!</div>
									<div class="relate_info">
										<span class="comment">Trả lời</span>
										<span class="like"><i class="icon_like"></i>Hài lòng</span>
										<span class="dislike"><i class="icon_dislike"></i>Không hài lòng</span>
										<span class="status">21 giờ 8 phút...</span>
									</div>

									<div class="comment_active comment_active_item">
										<form action="">
											<div class="comment_customer">
												<textarea id="" rows="1" cols="38" name="comment_customer" placeholder="Nhập bình luận của bạn..."  ></textarea>
											</div>
											<button type="button" class="cancel">Hủy</button>
											<button type="submit" class="sent">Gửi</button>
										</form>
									</div>
								</div>

							</div>
							<div class="pages border-bottom-0">
								<ul>
									<li>1</li>
									<li class="active">2</li>
									<li>3</li>
									<li>4</li>
									<li>5</li>
									<li>6</li>
									<li>...</li>
									<li>9</li>
								</ul>
							</div>
						</div>
						
						<div class="content_experience">
							<div class="experience">
								<h3 class="title_evaluate">Kinh nghiệm khi mua Tivi</h3>
								<div class="exp">
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_33.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>TOP 5 tivi 43 inch bán chạy nhất tháng 9/2019 tại Điện Máy Chợ TOP 5 tivi 43 inch bán chạy nhất tháng 9/2019 tại Điện Máy Chợ</p>
												</a>
											</div>
										</div>
									</div>
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_34.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Sharp cạnh tranh trực tiếp với LG trong phân khúc tivi màn hình...</p>
												</a>
											</div>
										</div>
									</div>
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_35.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tìm hiểu những điểm đặc biệt trên dòng tivi LG OLED 2019</p>
												</a>
											</div>
										</div>
									</div>
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_36.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tivi TCL Premium UHD AI C8 - Khi sự giải trí và công nghệ AI hòa làm một</p>
												</a>
											</div>
										</div>
									</div>

									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_33.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tivi TCL Premium UHD AI C8 - Khi sự giải trí và công nghệ AI hòa làm một</p>
												</a>
											</div>
										</div>
									</div>

									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_34.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tivi TCL Premium UHD AI C8 - Khi sự giải trí và công nghệ AI hòa làm một</p>
												</a>
											</div>
										</div>
									</div>
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_35.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tivi TCL Premium UHD AI C8 - Khi sự giải trí và công nghệ AI hòa làm một</p>
												</a>
											</div>
										</div>
									</div>
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_35.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tivi TCL Premium UHD AI C8 - Khi sự giải trí và công nghệ AI hòa làm một</p>
												</a>
											</div>
										</div>
									</div>
									<div class="one_exp">
										<div class="exp_item">
											<div class="image_exp">
												<a href="">
													<img src="assets/img/detail-product/detail_product_35.png" alt="">
												</a>
											</div>
											<div class="title_exp">
												<a href="">
													<p>Tivi TCL Premium UHD AI C8 - Khi sự giải trí và công nghệ AI hòa làm một</p>
												</a>
											</div>
										</div>
									</div>
								</div>
								<div class="see_more_write"><p>Xem thêm bài viết <i class="icon_arrow_right"></i></p></div>
							</div>
						</div>
					</div>
				</div>
			</div>		
		</div>
			
		<!--end content_page -->
		<div class="max-width">
			<div class="info_pro">
				<div class="info_pro_child">
					<div class="info_pro_child-img">
						<img src="assets/img/cat_con/cat_con_11.png" alt="">
					</div>
					<div class="info_pro_child-title">
						<h3>Bạn chưa tìm được sản phẩm Tivi ưng ý ngay hôm nay?</h3>
						<p>Chúng tôi sẽ gửi ngay cho bạn thông tin khuyến mãi trong thời gian sắp tới mà bạn quan tâm.</p>
					</div>
				</div>
				<div class="info_pro_item">
					<div class="email">
						<h3>ĐĂNG KÍ EMAIL</h3>
						<form action="">
							<div class="form_email">
								<input type="email" name="email_customer" value="" placeholder="Nhập địa chỉ mail của bạn…">
								<button type="submit">Đăng kí</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<!-- product_saw -->
		<div class="max-width">
			<div class="product_saw">
				<h3 class="title_pro">Sản phẫm đã xem </h3>
				<div class="slide_product owl-carousel owl-theme" id="product_saw">
					<div class="product">
						<div class="lable_pro">
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/hompage_17.png" alt="">
							</a>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png" alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>9.350.000đ</span>
						</div>
						<div class="gift">
							<span>Giảm 500.000đ và Quà 150.000đ</span>
						</div>
						<!-- <div class="discount">
							<span class="price_market">10.000.000đ</span>
							<span class="discount_percent">-10%</span>
							<span class="installment">Trả góp 0%</span>	
						</div> -->
						<div class="gift_detail">
							<!-- <span>Tặng máy pha cà phê</span> -->
						</div>
						<div class="evaluate_compare">
							
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/hompage_18.png" alt="">
							</a>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png" alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>19.590.000đ</span>
						</div>
						<div class="discount">
							<span class="price_market">24.590.000đ</span>
							<span class="discount_percent">-35%</span>
							<span class="installment">Trả góp 0%</span>	
						</div>
						<div class="gift_detail">
							<!-- <span>Tặng máy pha cà phê</span> -->
						</div>
						<div class="evaluate_compare">
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/hompage_19.png" alt="">
							</a>
						</div>
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png" alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>32.490.000đ</span>
						</div>
						<div class="discount">
							<span class="price_market">35.490.000đ</span>
							<span class="discount_percent">-15%</span>
						</div>
						<div class="gift_detail">
							<span>Tặng máy pha cà phê</span>
						</div>
						<div class="evaluate_compare">
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/hompage_20.png" alt="">
							</a>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png" alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>9.350.000đ</span>
						</div>
						<div class="gift">
							<span>Giảm 1.000.000đ </span>
						</div>
						<div class="gift_detail">
						</div>
						<div class="evaluate_compare">
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/hompage_21.png" alt="">
							</a>
						</div>
						
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png" alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>19.590.000đ</span>
						</div>
						<div class="discount">
							<span class="price_market">24.590.000đ</span>
							<span class="discount_percent">-35%</span>
							<span class="installment">Trả góp 0%</span>	
						</div>
						<div class="gift_detail">
						</div>
						<div class="evaluate_compare">
						</div>
					</div>
					<div class="product">
						<div class="lable_pro">
						</div>
						<div class="img_pro">
							<a href="">
								<img src="assets/img/hompage_09.png" alt="">
							</a>
						</div>
						<div class="name_pro">
							<a href="">
								<img src="assets/img/hompage_12.png" alt="" class="icon_trademark">
							</a>
							<a href="">
								<span>UA55NU7300 (2019)</span>
								<h3>Smart TV / Voice Seach / 43”</h3>
							</a>
							
						</div>
						<div class="price_sale">
							<span>9.350.000đ</span>
						</div>
						<div class="discount">
							<span class="price_market">12.350.000đ</span>
							<span class="discount_percent">-25%</span>
							<span class="installment">Trả góp 0%</span>	
						</div>
						<div class="gift_detail">
							<span>Tặng máy pha cà phê</span>
						</div>
						<div class="evaluate_compare">
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
		<div class="max-width">
			<div class="most_searched">
				<div class="most_searched_item">
					<p class="title_search">Tìm kiếm nhiều nhất</p>
					<a href="" class="download_more">Tải thêm <i></i></a>
					<div class="most_searched_child">
						<ul>
							<li>
								<a href="">
									<span><strong>#máy lạnh lg</strong>117 sản phẩm</span>
									<img src="assets/img/hompage_43.png" alt="">
								</a>
							</li>
							<li>
								<a href="">
									<span><strong>#smart tivi</strong> 1,9k sản phẩm</span>
									<img src="assets/img/hompage_44.png" alt="">
								</a>
							</li>
							<li>
								<a href="">
									<span><strong>#máy giặt 2019</strong> 189 sản phẩm</span>
									<img src="assets/img/hompage_45.png" alt="">
								</a>
							</li>
							<li>
								<a href="">
									<span><strong>#tủ lạnh Samsung</strong> 530 sản phẩm</span>
									<img src="assets/img/hompage_46.png" alt="">
								</a>
							</li>
							<li>
								<a href="">
									<span><strong>#nồi cơm điện</strong> 699 sản phẩm</span>
									<img src="assets/img/hompage_47.png" alt="">
								</a>
							</li>
							
						</ul>
					</div>
				</div>
			</div>
		</div>
		
		
	</section>
	<!-- footer -->
	<footer>
		<div class="max-width">
			<div class="phone_hotline">
				<a href="tel:02839505060" class="p_hotline_item">
					<i class="icon_security"></i>
					<span><strong>Bảo Hành: 028.39505060</strong> (8h30 - 21h00)</span>
				</a>
				<a href="tel:0283856 3388" class="p_hotline_item">
					<i class="icon_purchase"></i>
					<span><strong>Mua hàng: 028.3856 3388</strong> (8h30 - 21h00)</span>
				</a>
				<a href="tel:028.3856 3388" class="p_hotline_item">
					<i class="icon_complain"></i>
					<span><strong>Khiếu nại: 028.3856 3388</strong> (8h30 - 21h00)</span>
				</a>
			</div>
		</div>
		<div class="hotline_mb">
			<div class="hotline_mb_item">
				<a href="tel:0283856 3388" class="p_hotline_item">
					<img src="assets/img/hompage_55.png" alt="">
					<span>Mua hàng</span>
				</a>
				<a href="tel:02839505060" class="p_hotline_item">
					<img src="assets/img/hompage_56.png" alt="">
					<span>Bảo Hành</span>
				</a>
				<a href="tel:028.3856 3388" class="p_hotline_item">
					<img src="assets/img/hompage_57.png" alt="">
					<span>Khiếu nại</span>
				</a>
			</div>
			<a href="tel:02838563388" class="phone_hotline_mb">
				<span><strong>028.3856 3388</strong> (8h30 - 21h00)</span>
			</a>
		</div>
		<div class="max-width">
			<div class="general_infor_parent">
				<div class="general_infor">
					<ul class="info_item">
						<strong>THÔNG TIN CÔNG TY</strong>
						<li><a href="">Giới thiệu công ty</a></li>
						<li><a href="">Hệ thống siêu thị</a></li>
						<li><a href="">Phương châm bán hàng</a></li>
						<li><a href="">Cơ hội nghề nghiệp</a></li>
					</ul>
					<ul class="info_item">
						<strong>CHÍNH SÁCH CHUNG</strong>
						<li><a href="">Bảo trì - Bảo hành - Đổi trả</a></li>
						<li><a href="">Qui định giao hàng </a></li>
						<li><a href="">Điều khoản sử dụng</a></li>
						<li><a href="">Thoả thuận người dùng</a></li>
					</ul>
					<ul class="info_item">
						<strong>THẺ THÀNH VIÊN</strong>
						<li><a href="">Quyền lợi của Thành Viên</a></li>
						<li><a href="">Hỗ trợ Thành Viên</a></li>
						<li><a href="">Giftcard - Thẻ quà tặng</a></li>
						<li><a href="">Liên hệ</a></li>
					</ul>
					<ul class="info_item">
						<strong>MUA HÀNG ONLINE</strong>
						<li><a href="">Câu hỏi thường gặp</a></li>
						<li><a href="">Hướng dẫn mua hàng</a></li>
						<li><a href="">Lợi ích mua hàng online</a></li>
						<li><a href="">Tài khoản Khách hàng</a></li>
					</ul>
				</div>
				<div class="contact">
					<div class="social_network">
						<div class="social_network_item">
							<strong>KẾT NỐI VỚI CHÚNG TÔI</strong>
							<a href="" class="facebook"></a>
							<a href="" class="youtube"></a>
							<a href="" class="instagram"></a>
						</div>
					</div>
					<div class="branch">
						<div class="number_branch">
							<p>71</p>
						</div>
						<div class="info_branch">
							<div class="info_branch_item">
								<p>Chi nhánh trên<strong>TOÀN QUỐC</strong></p>
								<img src="assets/img/hompage_48.png" alt="ảnh location">
							</div>
							<a href="" class="add_branch_more">Xem thêm thông tin</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="info_footer">
			<div class="max-width">
				<div class="footer">
					<div class="footer_left">
						<h3>DIENMAYCHOLON.VN</h3>
						<p>© 2017. Công ty TNHH CAO PHONG .</p>
						<p>GPDKKD: 0302309845 do sở KH & ĐT TP.HCM cấp ngày 21/05/2001.</p>
						<p>Địa chỉ: Lô G chung cư Hùng Vương, Phường 11, Quận 5, TPHCM .</p>
						<p>Điện thoại: 028.39505060 - Email: info@dienmaycholon.com.vn.</p>
						<p>Chịu trách nhiệm nội dung: Trang Hồng Quân</p>
						<a href="" class="policy">Xem thêm chính sách bảo mật thông tin</a>
					</div>
					<div class="footer_right">
						<div class="certification">
							<p>Chứng nhận: </p>
							<ul>
								<li>
									<a href=""><img src="assets/img/hompage_49.png" alt="Đã thông báo bộ công thương"></a>
								</li>
								<li>
									<a href=""><img src="assets/img/hompage_50.png" alt="Đã đăng kí bộ công thương"></a>
								</li>
							</ul>
							
						</div>
						<div class="payment">
							<p>Thanh toán</p>
							<ul>
								<li><img src="assets/img/hompage_51.png" alt="Thanh toán COD"></li>
								<li><img src="assets/img/hompage_52.png" alt="Thanh toán Visa"></li>
								<li><img src="assets/img/hompage_53.png" alt="Thanh toán Moocash"></li>
							</ul>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</footer>
	<!-- popup images - videos -->

	<section class="popup_info " id="popup_images">
		<div class="popup_up">
			<div class="div_img">
				<img alt="Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300" src="assets/img/detail-product/detail_product_01.png">
			</div>
            <div class="div_pro_info">
                <h3>Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300</h3>
                <strong>5.990.000₫</strong>
                <span class="line-price">6.990.000₫</span>
            </div>
            <div class="button_container">
                <a href="#" class="buy_now">Mua ngay</a>
                <a href="#" class="buy_now repayment">Mua trả góp 0%</a>
                <a href="#" class="buy_now repayment">Trả góp qua thẻ</a>
            </div>
		</div>
		<div class="popup_down">
			<div class="button-close-popup"></div>
			<div class="popup_down-info">
				<div class="slide_popup-img-big owl-carousel" id="slide_popup-img-big">
					<div class="item">
						<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_02.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_03.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_02.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_03.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					
				</div>
				<div class="slide_popup-img-small owl-carousel" id="slide_popup-img-small">
					<div class="item">
						<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_02.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_03.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_02.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_03.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06.png" alt="ảnh Samsung Smart Tivi">
					</div>
					<div class="item">
						<img src="assets/img/detail-product/detail_product_06_01.png" alt="ảnh Samsung Smart Tivi">
					</div>
					
				</div>
			</div>
		</div>
	</section>	

	<section class="popup_info " id="popup_videos">
		<div class="popup_up">
			<div class="div_img">
				<img alt="Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300" src="assets/img/detail-product/detail_product_01.png">
			</div>
            <div class="div_pro_info">
                <h3>Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300</h3>
                <strong>5.990.000₫</strong>
                <span class="line-price">6.990.000₫</span>
            </div>
            <div class="button_container">
                <a href="#" class="buy_now">Mua ngay</a>
                <a href="#" class="buy_now repayment">Mua trả góp 0%</a>
                <a href="#" class="buy_now repayment">Trả góp qua thẻ</a>
            </div>
		</div>
		<div class="popup_down">
			<div class="button-close-popup"></div>
			<div class="popup_down-info">
				<div class="slide_popup-video-big">
					<iframe id="youtubeVideo" src="https://www.youtube.com/embed/fQSo5vimpvs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
				<div class="slide_popup-video-small">
					<div class="slide_popup-video-item">
						<a href="https://www.youtube.com/embed/fQSo5vimpvs" class="item active">
							<img src="assets/img/detail-product/detail_product_01.png" alt="ảnh Samsung Smart Tivi">
						</a>
						<a href="https://www.youtube.com/embed/tO2A3zQjKdU" class="item">
							<img src="assets/img/detail-product/detail_product_04.png" alt="ảnh Samsung Smart Tivi">
						</a>
						<a href="https://www.youtube.com/embed/7odBl90Azi0" class="item">
							<img src="assets/img/detail-product/detail_product_05.png" alt="ảnh Samsung Smart Tivi">
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>	

	<!-- popup number_market	 -->
	<div class="popup_st_parent">
		<div class="fuzzy_st"></div>
		<div class="popup_st">
			<div class="header_popup">
				<div class="button-close-popup"></div>
			</div>
			<div class="info_map">
				<div class="info_map_left">
					<div id="map_canvas" style="width:820px; height:550px;"></div>
				</div>
				<form action="">
				<div class="info_map_right">
					<div class="name_info_map">
						<h3>Samsung Smart TV / Voice Seach / 49”/ 2019 UA55NU7300</h3>
					</div>
					<div class="price_info_map">
						<p>Giá thị trường: <span class="pri_nor">6.990.000 Đ</span></p>
						<p>Giá bán: <span class="pri_dis">5.990.000 VNĐ</span></p>
					</div>
					<h4 class="number_maker">34 siêu thị còn hàng</h4>
					<div class="select_area">
						<div class="select_area_item">
							<select class="form_control" name="area" id="area">
	                            <option value="0" selected="selected">Tất Cả Khu Vực</option>
	                            <option value="1">TP Hồ Chí Minh</option>
	                            <option value="2">Các Tỉnh Miền Tây</option>
	                            <option value="3">Các Tỉnh Miền Trung</option>
	                        </select>
	                        <select class="form_control" name="county" id="county">
	                            <option selected="selected">Tất Cả Quận/Huyện</option> 
	                            <option value="4">QUẬN 5 (TRỤ SỞ CHÍNH)</option> 
	                            <option value="9">ĐỒNG NAI</option> 
	                            <option value="35">ĐỨC HÒA</option> 
	                            <option value="39">TRẢNG BOM - ĐỒNG NAI</option> 
	                            <option value="40">ĐỒNG NAI 4</option> 
	                            <option value="18">TÂN PHÚ</option> 
	                            <option value="19">QUẬN 12</option> 
	                            <option value="20">QUẬN 7</option> 
	                            <option value="21">QUẬN 4</option> 
	                            <option value="22">QUẬN 3</option> 
	                            <option value="23">QUẬN 2</option> 
	                            <option value="24">LONG KHÁNH - ĐỒNG NAI</option> 
	                            <option value="34">BÌNH DƯƠNG</option> 
	                            <option value="28">CỦ CHI</option> 
	                            <option value="41">BÌNH TÂN (HƯƠNG LỘ 2)</option> 
	                            <option value="44">BÌNH PHƯỚC</option> 
	                            <option value="47">VŨNG TÀU</option> 
	                            <option value="48">QUẬN 7 - (2)</option> 
	                            <option value="52">BÀ RỊA - VŨNG TÀU</option> 
	                            <option value="55">GÒ VẤP</option> 
	                            <option value="56">QUẬN 9</option> 
	                            <option value="57">BÌNH TÂN 2</option> 
	                            <option value="60">BÌNH DƯƠNG 2</option> 
	                            <option value="63">BÌNH DƯƠNG 3</option> 
	                            <option value="69">THỦ ĐỨC</option> 
	                            <option value="70">VĨNH LỘC</option> 
	                            <option value="72">HÓC MÔN</option> 
	                            <option value="73">TÂN AN</option> 
	                            <option value="74">NGUYỄN THỊ TÚ</option> 
	                            <option value="75">THỦ ĐỨC 2</option> 
	                            <option value="1">BẠC LIÊU</option> 
	                            <option value="5">CÀ MAU</option> 
	                            <option value="7">KIÊN GIANG</option> 
	                            <option value="37">CAI LẬY</option> 
	                            <option value="11">SÓC TRĂNG (CŨ)</option> 
	                            <option value="12">LONG XUYÊN - AN GIANG</option> 
	                            <option value="13">CẦN THƠ</option> 
	                            <option value="14">TRÀ VINH</option> 
	                            <option value="15">ĐỒNG THÁP</option> 
	                            <option value="16">VĨNH LONG 1</option> 
	                            <option value="17">MỸ THO – TIỀN GIANG</option> 
	                            <option value="26">BẾN TRE</option> 
	                            <option value="27">CẦN THƠ 2</option> 
	                            <option value="30">SA ĐÉC</option> 
	                            <option value="32">HẬU GIANG</option> 
	                            <option value="45">SÓC TRĂNG</option> 
	                            <option value="49">LONG XUYÊN 2</option> 
	                            <option value="58">GÒ CÔNG</option> 
	                            <option value="59">MỸ THO 2</option> 
	                            <option value="62">VĨNH LONG 2</option> 
	                            <option value="64">CÀ MAU 2</option> 
	                            <option value="71">CHÂU ĐỐC - AN GIANG</option> 
	                            <option value="2">ĐÀ NẴNG</option> 
	                            <option value="8">PHAN THIẾT</option> 
	                            <option value="36">ĐÀ NẴNG 2</option> 
	                            <option value="38">ĐÀ NẴNG 3</option> 
	                            <option value="33">BẢO LỘC</option> 
	                            <option value="43">QUY NHƠN</option> 
	                            <option value="46">ĐÀ LẠT</option> 
	                            <option value="50">GIA LAI</option> 
	                            <option value="51">PHAN RANG - NINH THUẬN</option> 
	                            <option value="54">KON TUM</option> 
	                            <option value="61">QUẢNG NGÃI</option> 
	                            <option value="65">NINH HÒA</option> 
	                            <option value="66">CAM RANH</option> 
	                            <option value="67">TAM KÌ</option> 
	                            <option value="68">PHÚ YÊN</option> 
	                            <option value="76">CHI NHÁNH DI LINH</option>
							</select>
						</div>
					</div>
					<div class="info_area">
						<ul>
							<li class="view_brand area_1 view_brand_t_4" >
								<label>
									<input type="radio" name="name_area" value="4" checked="checked" class="checked" data-longitude="10.75419" data-latitude="106.664557">
									<span class="name_county">QUẬN 5 ( TRỤ SỞ CHÍNH)</span>
									<span class="county_detail">Lô G,chung cư Hùng Vương, P.11</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_9" >
								<label>
									<input type="radio" name="name_area" value="9" data-longitude="10.957413" data-latitude="106.842687">
									<span class="name_county">ĐỒNG NAI</span>
									<span class="county_detail">260 b, phạm văn thuận, p. thống nhất, tp. biên hòa</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_35" >
								<label>
									<input type="radio" name="name_area" value="35" data-longitude="10.914974" data-latitude="106.384420">
									<span class="name_county">ĐỨC HÒA</span>
									<span class="county_detail">209 tỉnh lộ 825 , xã Đức hòa hạ , huyện Đức hòa , tỉnh long an</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_39" >
								<label>
									<input type="radio" name="name_area" value="39" data-longitude="10.965781" data-latitude="107.030062">
									<span class="name_county">TRẢNG BOM - ĐỒNG NAI</span>
									<span class="county_detail">số 433, quốc lộ 1a, Ấp thái hòa, xã hố nai, huyện trảng bom, tỉnh Đồng nai</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_40">
								<label>
									<input type="radio" name="name_area" value="40" data-longitude=" 10.9609573" data-latitude="106.8798433">
									<span class="name_county">ĐỒNG NAI 4</span>
									<span class="county_detail">số 753/2-4-6 xa lộ hà nội, khu phố 3, phường long bình, tp. biên hòa, tỉnh Đồng nai</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_18">
								<label>
									<input type="radio" name="name_area" value="18" data-longitude="10.801502" data-latitude="106.624841">
									<span class="name_county">TÂN PHÚ</span>
									<span class="county_detail">322 – 324 tân kỳ tân quý, p. sơn kỳ, q. tân phú, tp. hồ chí minh.</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_19">
								<label>
									<input type="radio" name="name_area" value="19" data-longitude="10.828469" data-latitude="106.624814">
									<span class="name_county">QUẬN 12</span>
									<span class="county_detail">189/4 trường chinh, p. tân hưng thuận</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_20">
								<label>
									<input type="radio" name="name_area" value="20" data-longitude="10.728333" data-latitude="106.718141">
									<span class="name_county">QUẬN 7</span>
									<span class="county_detail">101 tôn dật tiên, phú mỹ hưng</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_21">
								<label>
									<input type="radio" name="name_area" value="21" data-longitude="10.762211" data-latitude="106.701835">
									<span class="name_county">QUẬN 4</span>
									<span class="county_detail">siêu thị nội thất nhập khẩu simplehome việt nam, tòa nhà h2, 196 hoàng diệu, p.8 , q4</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_22">
								<label>
									<input type="radio" name="name_area" value="22" data-longitude="10.786831" data-latitude="106.665561">
									<span class="name_county">QUẬN 3</span>
									<span class="county_detail">590 cách mạng tháng tám, p.11, q.3</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_23">
								<label>
									<input type="radio" name="name_area" value="23" data-longitude="10.790921" data-latitude="106.745395">
									<span class="name_county">QUẬN 2</span>
									<span class="county_detail">tầng trệt, lầu 1, cao ốc lương Định của, p. an phú, q. 2</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_24">
								<label>
									<input type="radio" name="name_area" value="24" data-longitude="10.941703" data-latitude="107.233241">
									<span class="name_county">LONG KHÁNH - ĐỒNG NAI</span>
									<span class="county_detail">808a, Đường 21 tháng 4, núi tung, suối tre, long khánh, Đồng nai</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_34">
								<label>
									<input type="radio" name="name_area" value="34" data-longitude="10.973384" data-latitude="106.670588">
									<span class="name_county">BÌNH DƯƠNG</span>
									<span class="county_detail">283 Đại lộ bình dương, f. chánh nghĩa, tp. thủ dầu một</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_28">
								<label>
									<input type="radio" name="name_area" value="28" data-longitude="10.991997" data-latitude="106.449073">
									<span class="name_county">CỦ CHI</span>
									<span class="county_detail">535 quốc lộ 22 , khu phố 5 ,thị trấn củ chi,huyện củ chi,tp. hồ chí minh</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_41">
								<label>
									<input type="radio" name="name_area" value="41" data-longitude="10.769006" data-latitude="106.6094746">
									<span class="name_county">BÌNH TÂN (HƯƠNG LỘ 2)</span>
									<span class="county_detail">639 hương lộ 2,phường bình trị Đông,quận bình tân,tp. hồ chí minh.</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_44">
								<label>
									<input type="radio" name="name_area" value="44" data-longitude="11.515305" data-latitude="106.906177">
									<span class="name_county">BÌNH PHƯỚC</span>
									<span class="county_detail">658 phú riềng Đỏ, kp.tân trà, p. tân xuân, tx Đồng xoài, bình phước</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_47">
								<label>
									<input type="radio" name="name_area" value="47" data-longitude=" 10.3656103" data-latitude="107.0875503">
									<span class="name_county">VŨNG TÀU</span>
									<span class="county_detail">số 504 nguyễn an ninh, phường nguyễn an ninh, tp. vũng tàu</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_48">
								<label>
									<input type="radio" name="name_area" value="48" data-longitude="  10.7439647" data-latitude="106.7249378">
									<span class="name_county">QUẬN 7 - (2)</span>
									<span class="county_detail">520 huỳnh tấn phát, phường bình thuận, quận 7, tp.hcm</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_52">
								<label>
									<input type="radio" name="name_area" value="52" data-longitude="10.4911404" data-latitude="107.1848315">
									<span class="name_county">BÀ RỊA -VŨNG TÀU</span>
									<span class="county_detail">592 cách mạng tháng 8, phường phước trung, tp. bà rịa, tỉnh bà rịa-vũng tàu.</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_55">
								<label>
									<input type="radio" name="name_area" value="55" data-longitude="10.8473345" data-latitude="106.676289">
									<span class="name_county">GÒ VẤP</span>
									<span class="county_detail">531 nguyễn oanh, phường 17, quận gò vấp, tp.hcm</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_56">
								<label>
									<input type="radio" name="name_area" value="56" data-longitude="10.8272322" data-latitude="106.7669277">
									<span class="name_county">QUẬN 9</span>
									<span class="county_detail">103 tăng nhơn phú,phường phước long b, quận 9</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_57">
								<label>
									<input type="radio" name="name_area" value="57" data-longitude="10.7280753" data-latitude="106.6067788">
									<span class="name_county">BÌNH TÂN 2</span>
									<span class="county_detail">697 - 699 kinh dương vương, phường an lạc, quận bình tân, tphcm</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_60">
								<label>
									<input type="radio" name="name_area" value="60" data-longitude="10.8974726" data-latitude="106.7079067">
									<span class="name_county">BÌNH DƯƠNG 2</span>
									<span class="county_detail">27 bis quốc lộ 13, khu phố bình hòa, phường lái thiêu, thị xã thuận an, tỉnh bình dương</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_63">
								<label>
									<input type="radio" name="name_area" value="63" data-longitude="10.9709959" data-latitude="106.7206591">
									<span class="name_county">BÌNH DƯƠNG 3</span>
									<span class="county_detail">5a/2 Đường Đt 743, khu phố 1b, phường an phú, thị xã thuận an, tỉnh bình dương</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_69">
								<label>
									<input type="radio" name="name_area" value="69" data-longitude="10.8312479" data-latitude="106.7254757">
									<span class="name_county">THỦ ĐỨC</span>
									<span class="county_detail">tầng hầm b1, tòa nhà sense city, 240-242 phạm văn Đồng, phường hiệp bình chánh, quận thủ Đức</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_70">
								<label>
									<input type="radio" name="name_area" value="70" data-longitude="10.8310442" data-latitude="106.568687217">
									<span class="name_county">VĨNH LỘC</span>
									<span class="county_detail">f12/23b quách Điêu, Ấp 6,xã vĩnh lộc a,huyện bình chánh</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_72">
								<label>
									<input type="radio" name="name_area" value="72" data-longitude="10.867133" data-latitude="106.615848">
									<span class="name_county">HÓC MÔN</span>
									<span class="county_detail">475 Đường tô ký, Ấp nam thới, xã thới tam thôn, huyện hóc môn</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_73">
								<label>
									<input type="radio" name="name_area" value="73" data-longitude="10.537886" data-latitude="106.407299">
									<span class="name_county">TÂN AN</span>
									<span class="county_detail">số 7a, Đường trương Định, phường 2, thành phố tân an, long an</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_74">
								<label>
									<input type="radio" name="name_area" value="74" data-longitude="10.816038" data-latitude="106.599283">
									<span class="name_county">NGUYỄN THỊ TÚ</span>
									<span class="county_detail">41-43-45 nguyến thị tú, phường bình hưng hòa b, quận bình tân</span>
								</label>
							</li>
							<li class="view_brand area_1 view_brand_t_75">
								<label>
									<input type="radio" name="name_area" value="75" data-longitude="10.845525" data-latitude="106.749883">
									<span class="name_county">THỦ ĐỨC 2</span>
									<span class="county_detail">523b kha vạn cân, phường linh Đông, quận thủ Đức</span>
								</label>
							</li>
							<!-- brand -2 -->
							<li class="view_brand area_2 view_brand_t_1">
								<label>
									<input type="radio" name="name_area" value="1" class="checked" data-longitude="9.316628" data-latitude="105.718902">
									<span class="name_county">BẠC LIÊU</span>
									<span class="county_detail">135a/10 trần phú nối dài, p.7, tp. bạc liêu</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_5">
								<label>
									<input type="radio" name="name_area" value="5" data-longitude="9.152644" data-latitude="105.137166">
									<span class="name_county">CÀ MAU</span>
									<span class="county_detail">18 quốc lộ 1a ,Ấp bà Điều, xã lý văn lâm, tp.cà mau</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_7">
								<label>
									<input type="radio" name="name_area" value="7" data-longitude="9.966695" data-latitude="105.114266">
									<span class="name_county">KIÊN GIANG</span>
									<span class="county_detail">887 nguyễn trung trực, p. an bình, tp. rạch giá, kiên giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_37">
								<label>
									<input type="radio" name="name_area" value="37" data-longitude="10.405390" data-latitude="106.117351">
									<span class="name_county">CAI LẬY</span>
									<span class="county_detail">13/591 quốc lộ 1 ,phường 5, thị xã cai lậy, tỉnh tiền giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_11">
								<label>
									<input type="radio" name="name_area" value="11" data-longitude="9.62326" data-latitude="105.961497">
									<span class="name_county">SÓC TRĂNG (CŨ)</span>
									<span class="county_detail">khu dân cư minh châu, số 64a quốc lộ 1a, p. 7, tp. sóc trăng, tỉnh sóc trăng</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_12">
								<label>
									<input type="radio" name="name_area" value="12" data-longitude="10.370921" data-latitude="105.444478">
									<span class="name_county">LONG XUYÊN - AN GIANG</span>
									<span class="county_detail">số 151/1 trần hưng Đạo, p. mỹ phước, tp. long xuyên, an giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_13">
								<label>
									<input type="radio" name="name_area" value="13" data-longitude="10.046286" data-latitude="105.788285">
									<span class="name_county">CẦN THƠ</span>
									<span class="county_detail">số 108 trần văn khéo, p. cái khế, q. ninh kiều, tp. cần thơ</span>
								</label>
							</li>
							<li  class="view_brand area_2 view_brand_t_14">
								<label>
									<input type="radio" name="name_area" value="14" data-longitude="9.913772" data-latitude="106.315145">
									<span class="name_county">TRÀ VINH</span>
									<span class="county_detail">nguyễn thị minh khai, khóm 6, p. 8, tp. trà vinh, tỉnh trà vinh</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_15">
								<label>
									<input type="radio" name="name_area" value="15" data-longitude="10.457723" data-latitude="105.650064">
									<span class="name_county">ĐỒNG THÁP</span>
									<span class="county_detail">khu dân cư mỹ trà, p. mỹ phú, tp. cao lãnh, Đồng tháp</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_16">
								<label>
									<input type="radio" name="name_area" value="16" data-longitude="10.254811" data-latitude="105.970783">
									<span class="name_county">VĨNH LONG 1</span>
									<span class="county_detail">siêu thị co.opmart vĩnh long, số 26 đường 3/2, p. 1, tx. vĩnh long, tỉnh vĩnh long.</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_17">
								<label>
									<input type="radio" name="name_area" value="17" data-longitude="10.370626" data-latitude="106.349648">
									<span class="name_county">MỸ THO - TIỀN GIANG</span>
									<span class="county_detail">số 35, Ấp bắc, p. 5, tp. mỹ tho, tiền giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_26">
								<label>
									<input type="radio" name="name_area" value="26" data-longitude="10.258474" data-latitude="106.362043">
									<span class="name_county">BẾN TRE</span>
									<span class="county_detail">171d Đường võ nguyên giáp, xã bình phú, tp. bến tre</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_27">
								<label>
									<input type="radio" name="name_area" value="27" data-longitude="10.018617" data-latitude="105.762709">
									<span class="name_county">CẦN THƠ 2</span>
									<span class="county_detail">155-157-159-161 Đường 3 tháng 2, f. hưng lợi, q. ninh kiều, tp. cần thơ</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_30">
								<label>
									<input type="radio" name="name_area" value="30" data-longitude="10.290203" data-latitude="105.754316">
									<span class="name_county">SA ĐÉC</span>
									<span class="county_detail">số 393 , nguyễn sinh sắc , phường 2 ,thành phố sa Đéc , tỉnh Đồng tháp</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_32">
								<label>
									<input type="radio" name="name_area" value="32" data-longitude="9.798136" data-latitude="105.491151">
									<span class="name_county">HẬU GIANG</span>
									<span class="county_detail">khu vực 2 , Đường võ văn kiệt , phường 5 , tp. vị thanh , hậu giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_45">
								<label>
									<input type="radio" name="name_area" value="45" data-longitude="9.6235322" data-latitude="105.491151">
									<span class="name_county">SÓC TRĂNG</span>
									<span class="county_detail">số 217 quốc lộ 1a, phường 7, tp. sóc trăng, tỉnh sóc trăng</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_49">
								<label>
									<input type="radio" name="name_area" value="49" data-longitude="10.350556" data-latitude="105.4591813">
									<span class="name_county">LONG XUYÊN 2</span>
									<span class="county_detail">45/11 trần hưng Đạo, phường mỹ thạnh,tp. long xuyên, tỉnh an giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_58">
								<label>
									<input type="radio" name="name_area" value="58" data-longitude="10.3766293" data-latitude="106.6801066">
									<span class="name_county">GÒ CÔNG</span>
									<span class="county_detail">Ấp hưng hòa, xã long hưng, thị xã gò công, tỉnh tiền giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_59">
								<label>
									<input type="radio" name="name_area" value="59" data-longitude="10.373008" data-latitude="106.342127">
									<span class="name_county">MỸ THO 2</span>
									<span class="county_detail">số 77 nguyễn thị thập, khu phố 4, phường 10, thành phố mỹ tho, tỉnh tiền giang</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_62">
								<label>
									<input type="radio" name="name_area" value="62" data-longitude="10.1961604" data-latitude="106.004943">
									<span class="name_county">VĨNH LONG 2</span>
									<span class="county_detail">số 310, quốc lộ 53, thị trấn long hồ, huyện long hồ, tỉnh vĩnh long</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_64">
								<label>
									<input type="radio" name="name_area" value="64" data-longitude="9.1735099" data-latitude="105.1535759">
									<span class="name_county">CÀ MAU 2</span>
									<span class="county_detail">103-105-107 lý thường kiệt, phường 6, thành phố cà mau,tỉnh cà mau</span>
								</label>
							</li>
							<li class="view_brand area_2 view_brand_t_71">
								<label>
									<input type="radio" name="name_area" value="71" data-longitude="10.694563" data-latitude="105.098535">
									<span class="name_county">CHÂU ĐỐC - AN GIANG</span>
									<span class="county_detail">394-396 tân lộ kiều lương, khóm 8, phường châu phú a, tp châu Đốc - an giang</span>
								</label>
							</li>

							<!-- BRAND -3 -->
							<li class="view_brand area_3 view_brand_t_2">
								<label>
									<input type="radio" name="name_area" value="2" class="checked" data-longitude="16.031666" data-latitude="108.20917">
									<span class="name_county">ĐÀ NẴNG</span>
									<span class="county_detail">559 nguyễn hữu thọ, p. khuê trung, q. cẩm lệ, tp. Đà nẵng</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_8">
								<label>
									<input type="radio" name="name_area" value="8" data-longitude="10.938764" data-latitude="108.110444">
									<span class="name_county">PHAN THIẾT</span>
									<span class="county_detail">b2 khu dân cư hùng vương 2, p. phú thủy, tp phan thiết, tỉnh bình thuận</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_36">
								<label>
									<input type="radio" name="name_area" value="36" data-longitude="16.061738" data-latitude="108.204301">
									<span class="name_county">ĐÀ NẴNG 2</span>
									<span class="county_detail">116-118-120-122 nguyễn tri phương , phường chính gián ,quận thanh khê , tp. Đà nẵng</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_38">
								<label>
									<input type="radio" name="name_area" value="38" data-longitude="16.056694" data-latitude="108.167685">
									<span class="name_county">ĐÀ NẴNG 3</span>
									<span class="county_detail">403 tôn Đức thắng, phường hòa minh, quận liên chiểu, tp. Đà nẵng</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_33">
								<label>
									<input type="radio" name="name_area" value="33" data-longitude="11.535849" data-latitude="107.817056">
									<span class="name_county">BẢO LỘC</span>
									<span class="county_detail">số 252 trần phú , phường lộc sơn , tp.bảo lộc, tỉnh lâm Đồng</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_43">
								<label>
									<input type="radio" name="name_area" value="43" data-longitude="13.766015" data-latitude="109.215411">
									<span class="name_county">QUY NHƠN</span>
									<span class="county_detail">tổ 17, kv4, đường nguyễn thái học (khu c chqs tỉnh), phường nguyễn văn cừ, thành phố quy nhơn, tỉnh bình Định</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_46">
								<label>
									<input type="radio" name="name_area" value="46" data-longitude="11.9322092" data-latitude="108.4437163">
									<span class="name_county">ĐÀ LẠT</span>
									<span class="county_detail">số 17 Đường 3/4, phường 3, tp. Đà lạt</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_50">
								<label>
									<input type="radio" name="name_area" value="50" data-longitude="13.9837429" data-latitude="107.9994016">
									<span class="name_county">GIA LAI</span>
									<span class="county_detail">100 phan Đình phùng, phường tây sơn, thành phố pleiku, tỉnh gia lai,việt nam</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_51">
								<label>
									<input type="radio" name="name_area" value="51" data-longitude="11.5581271" data-latitude="108.9878748">
									<span class="name_county">PHAN RANG - NINH THUẬN</span>
									<span class="county_detail">số 632 Đường thống nhất,phường Đạo long, tp.phan rang - tháp chàm, tỉnh ninh thuận</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_54">
								<label>
									<input type="radio" name="name_area" value="54" data-longitude="14.3508176" data-latitude="108.0023502">
									<span class="name_county">KON TUM</span>
									<span class="county_detail">38 hoàng văn thụ, phường quyết thắng, thành phố kon tum, tỉnh kon tum</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_61">
								<label>
									<input type="radio" name="name_area" value="61" data-longitude="15.113436" data-latitude="108.816977">
									<span class="name_county">QUÃNG NGÃI</span>
									<span class="county_detail">126 Đinh tiên hoàng, phường nghĩa chánh, tp. quảng ngãi, tỉnh quảng ngãi</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_65">
								<label>
									<input type="radio" name="name_area" value="65" data-longitude="12.489844" data-latitude="109.126165">
									<span class="name_county">NINH HÒA</span>
									<span class="county_detail">Đường nguyễn thị ngọc oanh,phường ninh hiệp, thị xã ninh hòa,tỉnh khánh hòa</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_66">
								<label>
									<input type="radio" name="name_area" value="66" data-longitude="11.919484" data-latitude="109.1565499">
									<span class="name_county">CAM RANH</span>
									<span class="county_detail">1919 Đường hùng vương, phường cam phú, thành phố cam ranh, tỉnh khánh hòa</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_67">
								<label>
									<input type="radio" name="name_area" value="67" data-longitude="15.5590908" data-latitude="108.5006016">
									<span class="name_county">TAM KỲ</span>
									<span class="county_detail">832-834 đường phan châu trinh,phường an sơn, thành phố tam kỳ,tỉnh quảng nam</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_68">
								<label>
									<input type="radio" name="name_area" value="68" data-longitude="13.0853216" data-latitude="109.2953571,17">
									<span class="name_county">PHÚ YÊN</span>
									<span class="county_detail">số 02-04 lê lợi, phường 1, tp tuy hòa,tỉnh phú yên</span>
								</label>
							</li>
							<li class="view_brand area_3 view_brand_t_76">
								<label>
									<input type="radio" name="name_area" value="76" data-longitude="11.592276" data-latitude="108.078706">
									<span class="name_county">CHI NHÁNH DI LINH</span>
									<span class="county_detail">1080 hùng vương, thị trấn di linh, huyện di linh, tỉnh lâm Đồng</span>
								</label>
							</li>
							
						</ul>
						
					</div>
					<div class="button_booking_map">
						<button type="submit"><span>ĐẶT GIỮ HÀNG TẠI SIÊU THỊ  NÀY</span></button>
					</div>
					<div class="hotline_map">
						<a href="tel:0838563388">(08) 3856 3388</a>
						<span>(Online từ 8h30 đến 21h00 từ Thứ 2 - Chủ Nhật)</span>
					</div>
				</div>
				</form>
			</div>
		</div>	
	</div>
</body>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true"></script>
<script type="text/javascript" src="public/js/front-end.js"></script>
<!-- <script type="text/javascript" src="public/js/slider-product-react.js"></script> -->
<script src="public/js/slider-product-react.js"></script>