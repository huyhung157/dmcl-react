let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */

// mix.js('assets/js/front-end.js', 'public/js/front-end.js');

mix.react('react/app/index.js', 'public/js/slider-product-react.js');
mix.sass('assets/css/front-end.scss', 'public/css/front-end.css');
mix.sass('react/app/App.scss', 'public/css/slider-product-react.css');